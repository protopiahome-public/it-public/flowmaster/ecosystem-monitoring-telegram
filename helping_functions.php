<?php
function getThemeIdByTitle($title)
{
	$themes = protopia_query("getThemes", "_id title");
	
	foreach ($themes as $theme)
	{
		if (mb_strtolower($theme["title"]) == mb_strtolower($title))
		{
			return $theme["_id"];
		}
	}
	
	return null;
}

function getSourceIdByTitle($title)
{
	$sources = protopia_query("getSources", "_id title");
	
	foreach ($sources as $source)
	{
		if (mb_strtolower($source["title"]) == mb_strtolower($title))
		{
			return $source["_id"];
		}
	}
	
	return null;
}

function getReceiverIdByTitle($title)
{
	$receivers = protopia_query("getReceivers", "_id title");
	
	foreach ($receivers as $receiver)
	{
		if (mb_strtolower($receiver["title"]) == mb_strtolower($title))
		{
			return $receiver["_id"];
		}
	}
	
	return null;
}

function getGroupIdByTitle($title)
{
	$sources = protopia_query("getGroups", "_id title");
	
	foreach ($sources as $source)
	{
		if (mb_strtolower($source["title"]) == mb_strtolower($title))
		{
			return $source["_id"];
		}
	}
	
	return null;
}

function getUserIdByTelegramId($user)
{
	global $ecosystem_client_auth;
	$ecosystem_client_auth = true;
	$result = protopia_query("getUserByExternalId", "_id", [
		"external_id: String" => $user,
		"external_system: String" => "telegram",
	]);
	$ecosystem_client_auth = false;
	return $result["_id"];
}

function getReceiverByExternal()
{
	global $ecosystem_client_auth, $data;
	$ecosystem_client_auth = true;
	$result = protopia_query("getReceiverByExternal", "_id", [
		"external_id: String" => $data["message"]["chat"]["id"],
		"external_type: String" => $data["message"]["chat"]["id"] == $data["message"]["from"]["id"] ? "personal_chat" : "group_chat",
		"external_system: String" => "telegram",
	]);
	
	$ecosystem_client_auth = false;
	return $result["_id"];
}
function getSourceByExternal($chat_id = null)
{
	global $ecosystem_client_auth, $data;
	$ecosystem_client_auth = true;
	if (!$chat_id) {
		$chat_id = $data["message"]["chat"]["id"];
	}
	$result = protopia_query("getSourceByExternal", "_id", [
		"external_id: String" => $chat_id,
		"external_type: String" => $chat_id == $data["message"]["from"]["id"] ? "personal_chat" : "group_chat",
		"external_system: String" => "telegram",
	]);
	$ecosystem_client_auth = false;
	return $result["_id"];
}

function createLinkById($id)
{
	global $ecosystem_client_auth, $data;
	$ecosystem_client_auth = true;
	$result = protopia_mutation("createLinkById", "", [
		"id: ID!" => $id,
	]);
	$ecosystem_client_auth = false;
	return $result;
}

function getIdByLink($link)
{
	global $ecosystem_client_auth, $data;
	$ecosystem_client_auth = true;
	$result = protopia_query("getIdByLink", "", [
		"link: String!" => $link,
	]);
	answer($result);
	$ecosystem_client_auth = false;
	return $result;
}

function getIdByLinkOrTitle($string, $type)
{
	preg_match("/^#link(.+)$/", $string, $matches);
	if ($matches[1])
	{
		return getIdByLink($matches[1]);
	}
	if ($type == "source")
	{
		return getSourceIdByTitle($string);
	}
	if ($type == "theme")
	{
		return getThemeIdByTitle($string);
	}
	if ($type == "receiver")
	{
		return getReceiverIdByTitle($string);
	}
}

function getSubscriptions()
{
	global $data;
	
	$text = "";
	
	$receiver = protopia_query("getReceiver", "_id activate_sources {title} themes {title keywords activate}", ["id: ID!" => getReceiverByExternal()]);
	
	if ($receiver)
	{
		$text = "";
		if ($receiver["themes"])
		{
			$text .= "Темы:\n";
			foreach ($receiver["themes"] as $theme)
			{
				$status = $theme["activate"] ? "активна" : "неактивна";
				$text .= "----{$theme["title"]} (" . implode(", ", $theme["keywords"]) . ") [" . $status . "]\n";
			}
		}
		if ($receiver["activate_sources"])
		{
			$text .= "Активированные источники:\n";
			foreach ($receiver["activate_sources"] as $source)
			{
				$text .= "----{$source["title"]}\n";
			}
		}
	}
	if (!count($receiver["themes"]) && !count($receiver["activate_sources"]))
	{
		$text = "Подписки отсутствуют";
	}
	
	return $text;
}
