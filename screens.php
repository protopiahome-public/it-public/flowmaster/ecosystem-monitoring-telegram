<?php
function change_list ($layout, $button_pressed) {
    $breadcrumbs_array = explode("/", $_SESSION["breadcrumbs"]);
    if (intval($breadcrumbs_array[(count($breadcrumbs_array))])=="h"){
        notification_to_callback_query ();
    }
    $current_screen = intval($breadcrumbs_array[(count($breadcrumbs_array)-1)]);
switch ($current_screen) {
    case 3:
        $total = protopia_query("getMySources", "_id title");
        $buttons = regenerate_lister(4, $total, $layout, $button_pressed);
        edit_message_buttons($buttons);
        notification_to_callback_query ();
        break;
    case 6:
        $total = protopia_query("getReceivers", "_id title");
        $buttons = regenerate_lister("4ar", $total, $layout, $button_pressed);
        array_unshift($buttons["inline_keyboard"], array(array("text"=>"\xf0\x9f\x94\xa2Добавить по коду", "callback_data"=>"7:")
        ));
        edit_message_buttons($buttons);
        notification_to_callback_query ();
        break;
    case 8:
        $almost_total = protopia_query("getSource", "shared_to_receivers {_id title}", ["id: ID!" => $_SESSION["id1"]]);
        $buttons = regenerate_lister("4rr", $almost_total["shared_to_receivers"], $layout, $button_pressed);
        edit_message_buttons($buttons);
        notification_to_callback_query ();
        break;
    case 10:
        $total = protopia_query("getMyReceivers", "_id title");
        $buttons = regenerate_lister(11, $total, $layout, $button_pressed);
        edit_message_buttons($buttons);
        notification_to_callback_query ();
        break;
    case 13:
        $almost_total =  protopia_query("getReceiver", "activate_sources {_id title}", ["id: ID!" => $_SESSION["id1"]]);
        $buttons = regenerate_lister("11ds", $almost_total["activate_sources"], $layout, $button_pressed);
        edit_message_buttons($buttons);
        notification_to_callback_query ();
        break;
    case 16:
        $almost_total = protopia_query("getReceiver", "themes {_id title}", ["id: ID!" => $_SESSION["id1"]]);
        $buttons = regenerate_lister("25", $almost_total["themes"], $layout, $button_pressed);
        array_unshift($buttons["inline_keyboard"], array(array("text"=>"\xf0\x9f\x93\x83Добавить новую тему", "callback_data"=>"17:")
        ));
        edit_message_buttons($buttons);
        notification_to_callback_query ();
        break;
    case 19:
        $total = protopia_query("getMyReceivers", "_id title");
        edit_message_buttons(regenerate_lister("20:", $total, $layout, $button_pressed));
        break;
    case 20:
        $almost_total = protopia_query("getReceiver", "themes {_id title}", ["id: ID!" => $_SESSION["id2"]]);
        edit_message_buttons(regenerate_lister("25:cf", $almost_total["themes"], $layout, $button_pressed));
        break;
    case 21:
        $almost_total = protopia_query("getSources", "_id title");
        $minus = protopia_query("getReceiver", "activate_sources {_id title}", ["id: ID!" => $_SESSION["id1"]]);
        $total = minus_second_array($almost_total, $minus["activate_sources"]);
        $buttons = regenerate_lister("11as", $total, $layout, $button_pressed);
        array_unshift($buttons["inline_keyboard"], array(array("text"=>"\xf0\x9f\x94\xa2Сгенерировать код-приглашение", "callback_data"=>"22:")
        ));
        edit_message_buttons($buttons);
        notification_to_callback_query ();
        break;
    case 23:
        $total = protopia_query("getThemes", "_id title keywords receiver {_id title}"); //Достать темы
        $buttons = regenerate_slash_lister("25tl", "25tl", $total, $layout, $button_pressed);
        edit_message_buttons($buttons);
        notification_to_callback_query ();
        break;
    case 24:
        $total = protopia_query("getThemes", "_id title keywords receiver {_id title}"); //Достать темы
        $receivers = protopia_query("getMyReceivers", "_id title");//Достать список моих источников

        foreach ($total as $key => $theme) {
            foreach ($receivers as $receiver) {
                if ($receiver["_id"]==$theme["receiver"]["_id"]) {
               $total[$key]["editable"]=true;
                }
            }
        }
        //Сравнить список тем. Если в списке тем есть receiver из списка моих receiverов, добавить поле editable
        $buttons = regenerate_slash_lister("25", "31", $total, $layout, $button_pressed);
        edit_message_buttons($buttons);
        notification_to_callback_query ();
        break;
    case 28:
        $almost_total = protopia_query("getTheme", "keywords", ["id: ID!" => $_SESSION["id2"]]);
        $buttons = regenerate_keyword_lister("25dw", $almost_total["keywords"],$layout, $button_pressed);
        edit_message_buttons($buttons);
        break;
    case 31:
        $total = protopia_query("getMyReceivers", "_id title");
        $buttons = generate_lister("25cr", $total);
        edit_message_buttons($buttons);
        break;
    default:
        answer("Ошибка перелистывателя");
        notification_to_callback_query ();
}
}

function registration01 () {
    $text = "Добро пожаловать в Flowmaster, систему чат-мониторинга. 

Система мониторит сообщения в чатах-источниках и пересылает их в чаты-приёмники в соответствии с установленными темами.

Для того, чтобы начать пользоваться ботом, выполните 3 шага: 
1) Зарегистрируйтесь или войдите на сайт flowmaster.info;
2) Перейдите в отдел \"Войти во внешних системах\" и нажмите вариант \"Telegram\";
3) Скопируйте полученную команду и пришлите её боту.
";
    $buttons = array("inline_keyboard"=>array(array(array("text"=>"\xe2\x86\x97\xef\xb8\x8fПерейти на сайт", "url"=>"flowmaster.info"))));
    answer($text,$buttons);
}

function mainmenu02 ($last_action = null, $answer_not_edit = false) {
    unset($_SESSION["id1"]);
    unset($_SESSION["id2"]);
    unset($_SESSION["user_state"]);
    unset($_SESSION["breadcrumbs"]);
    $_SESSION["breadcrumbs"] = "2";
    global $data;
    if (boolval($data["message"]["text"])){
    $receiver = protopia_query("getReceiver", "_id is_disabled", ["id: ID!" => getReceiverByExternal()]);
    } else {
    $receiver = protopia_query("getReceiver", "_id is_disabled", ["id: ID!" => getReceiverByExternalCallback()]);
    }
    if (!$receiver["is_disabled"]&&$receiver["_id"]){
        $is_a_receiver_button = array("text"=>"\xe2\x9a\x99\xef\xb8\x8fНастройки этого приёмника", "callback_data"=>"11:{$receiver["_id"]}");
        } else {
	$is_a_receiver_button = array("text"=>"\xf0\x9f\x8e\xa7Сделать этот чат приёмником", "callback_data"=>"11mr:");
        }
    $text = "{$last_action}Добро пожаловать в меню управления FlowMaster. Добавьте бота в чат, чтобы он мог сделать из него источник или приёмник.";
    $buttons = array("inline_keyboard"=>array(
	array(array("text"=>"\xe2\x86\x97\xef\xb8\x8fВойти в вебпанель", "callback_data"=>"link")), 
	array(array("text"=>"\xf0\x9f\x93\xa1Мои источники", "callback_data"=>"3:")),
        array(array("text"=>"\xf0\x9f\x93\xbbМои приёмники", "callback_data"=>"10:")),
        array(array("text"=>"\xf0\x9f\x93\x8bМои темы", "callback_data"=>"24:")),
	array($is_a_receiver_button)
        ));
    if ($answer_not_edit) {
        edit_message_buttons(null, $_SESSION["menu_message"]["result"]["chat"]["id"], $_SESSION["menu_message"]["result"]["message_id"]);
        $_SESSION["menu_message"] = answer($text,$buttons);
    } else {
        edit_message_text($text, $buttons);
    }
}

function sourceslist03 () {    
    $total = protopia_query("getMySources", "_id title is_disabled");
    foreach ($total as $key => $source){
        if ($source["is_disabled"]) {
            unset ($total[$key]);
        }
    } 
    $total = array_values($total);
    if (count($total)==0) {
        notification_to_callback_query ("У вас нет источников. Добавьте бота в чат и нажмите /monitor");
    } else {
        add_to_breadcrumbs(3);
        $text = "Вот список источников, которыми вы можете управлять. Выберите источник, чтобы получить о нём подробную информацию";
        edit_message_text($text, generate_lister(4, $total));
    }
}

function sourcemenupoint04 ($id, $lastaction = null, $answer_not_edit = false) {
    $_SESSION["id1"] = $id;
    $source = protopia_query("getSource", "_id title is_public shared_to_receivers {_id title}", ["id: ID!" => $id]);
    add_to_breadcrumbs (4);
    $is_public_word = is_public_word(boolval($source["is_public"]));
    $shared_to_receivers = text_list($source["shared_to_receivers"]);
    $text = "{$lastaction}Информация о источнике <b>{$source["title"]}</b>\n\nПубличный: {$is_public_word}\nПодключенные приёмники:{$shared_to_receivers}";
    if (!$source["is_public"]){
    $is_public = array("text"=>"\xf0\x9f\x94\x93Сделать источник публичным", "callback_data"=>"4mp:");
    } else {
    $is_public = array("text"=>"\xf0\x9f\x94\x92Сделать источник непубличным", "callback_data"=>"4mu:");
    }
    $buttons = array("inline_keyboard"=>array(
        array($is_public),
        array(array("text"=>"\xe2\x9e\x95Добавить новый приёмник", "callback_data"=>"6:")),
        array(array("text"=>"\xf0\x9f\x9a\xabРазорвать связь с приёмником", "callback_data"=>"8:")),
        array(array("text"=>"\xe2\x9d\x8cДеактивировать источник", "callback_data"=>"5:")),
        generate_back_buttons($id)
        ));
    if ($answer_not_edit) {
        edit_message_buttons(null, $_SESSION["menu_message"]["result"]["chat"]["id"], $_SESSION["menu_message"]["result"]["message_id"]);
        $_SESSION["menu_message"] = answer($text, $buttons, "HTML");
    } else {
        edit_message_text($text, $buttons, "HTML");
    }
}

function deactivatesource05 () {
    $text = "Вы уверены, что хотите деактивировать источник? Он пропадет из списка доступных источников, и для возвращения его придётся использовать команду /monitor";
    $buttons = array("inline_keyboard"=>array(
        array(array("text"=>"\xe2\x98\x91\xef\xb8\x8fДа", "callback_data"=>"02ds:")),
        array(array("text"=>"\xe2\x9d\x8cОтмена", "callback_data"=>"4:{$_SESSION["id1"]}:back"))
        ));
    edit_message_text($text, $buttons, "HTML");
}

function addrecieverlist06 () {
    add_to_breadcrumbs (6);
    $almost_total = protopia_query("getReceivers", "_id title");
    $minus = protopia_query("getSource", "shared_to_receivers {_id title}", ["id: ID!" => "{$_SESSION["id1"]}"]);
    $total = minus_second_array($almost_total, $minus["shared_to_receivers"]);
    $buttons = generate_lister("4ar", $total);
    array_unshift($buttons["inline_keyboard"], array(array("text"=>"\xf0\x9f\x94\xa2Добавить по коду", "callback_data"=>"7:")
        ));
    $text = "Выберите приёмник, чтобы добавить его. Если хотите добавить приёмник, который принадлежит не вам, нажмите \"Добавить по коду\"";
    edit_message_text($text, $buttons);
}

function addbycode07() { 
    add_to_breadcrumbs (7);
    $_SESSION["user_state"] = "waitforreceivercode";
    $text = "Введите код приёмника, который хотите добавить";
    $buttons = $buttons = array("inline_keyboard"=>array(array(array("text"=>"\xe2\x9d\x8cОтмена", "callback_data"=>"04:{$_SESSION["id1"]}:back"))));
    edit_message_text($text, $buttons);
}

function removerecieverlist08 () {
    $almost_total = protopia_query("getSource", "shared_to_receivers {_id title}", ["id: ID!" => "{$_SESSION["id1"]}"]);
    if (count($almost_total["shared_to_receivers"])==0) {
        notification_to_callback_query ("У этого источника нет приёмников.");
    } else {
    add_to_breadcrumbs (8);
      $text = "Выберите приёмник, чтобы разорвать связь с ним.";
    edit_message_text($text, generate_lister("9", $almost_total["shared_to_receivers"]));
    }
}

function approveremovingreceiver09 ($id) {
    $source = protopia_query("getSource", "title", ["id: ID!" => $_SESSION["id1"]]);
    $receiver = protopia_query("getReceiver", "title", ["id: ID!" => $id]);
    $text = "Вы уверены, что хотите разорвать связь между источником <b>{$source["title"]}</b> и приёмником <b>{$receiver["title"]}</b>";
    $buttons = array("inline_keyboard"=>array(
        array(array("text"=>"\xe2\x98\x91\xef\xb8\x8fДа", "callback_data"=>"4rr:$id")),
      array(array("text"=>"\xe2\x9d\x8cОтмена", "callback_data"=>"8:{$_SESSION["id1"]}:back"))
        ));
    edit_message_text ($text, $buttons, "HTML");
}

function receiverslist10 () {
    $total = protopia_query("getMyReceivers", "_id title is_disabled");
    foreach ($total as $key => $receiver){
        if ($receiver["is_disabled"]) {
            unset ($total[$key]);
        }
    } 
    $total = array_values($total);
    if (count($total)==0) {
        notification_to_callback_query ("У вас нет приёмников. Добавьте бота в чат и нажмите /receive");
    } else {
    add_to_breadcrumbs (10);
    $text = "Вот список приёмников, которыми вы можете управлять. Выберите приёмник, чтобы получить о нём подробную информацию";
    edit_message_text($text, generate_lister(11, $total));
    }
    }

function receivermenupoint11 ($id, $lastaction = null, $answer_not_edit = false) {
    $_SESSION["id1"] = $id;
    $receiver = protopia_query("getReceiver", "_id title is_public activate_sources {_id title} themes {title keywords activate}", ["id: ID!" => $id]);
    //$all_sources = protopia_query("getSources", "_id title");
    //$available_sources = array_diff($all_sources, $receiver["activate_sources"]); СЕЙЧАС НЕ АКТУАЛЬНО
    add_to_breadcrumbs (11);
    $is_public_word = is_public_word(boolval($receiver["is_public"]));
    $activate_sources = text_list($receiver["activate_sources"]);
    /*if (count($available_sources)==0){ //Трактовка списка доступных приёмников СЕЙЧАС НЕ АКТУАЛЬНО
    $available_sources_word = " Нет";
    } else {
    foreach ($available_sources as $source) {
        $available_sources_word = $available_sources_word . "\n        \xf0\x9f\x94\x87{$source["title"]}";
    }}*/
    if (count($receiver["themes"])==0){ //Трактовка списка тем
        $themes = " Нет";
    } else {
    foreach ($receiver["themes"] as $theme) {
        if ($theme["activate"]) {
            $active = "\xf0\x9f\x94\x94";
        } else {
            $active = "\xf0\x9f\x94\x95";
        }
        $themes = $themes . "\n        {$active} {$theme["title"]}";
    }
    }
    $text = "{$lastaction}Информация о приёмнике <b>{$receiver["title"]}</b>\n\nПубличный: {$is_public_word}\nАктивированные источники:{$activate_sources}\nСвязанные темы:{$themes}";
if (!$receiver["is_public"]){ //Создание кнопки про публичность
    $is_public = array("text"=>"\xf0\x9f\x94\x93Сделать приёмник публичным", "callback_data"=>"11mp:");
} else {
    $is_public = array("text"=>"\xf0\x9f\x94\x92Сделать приёмник непубличным", "callback_data"=>"11mu:");
}
    $buttons = array("inline_keyboard"=>array(
        array($is_public),
        array(array("text"=>"\xe2\x9e\x95Подключиться к новому источнику", "callback_data"=>"21:")),
        array(array("text"=>"\xf0\x9f\x94\x87Деактивировать один из источников", "callback_data"=>"13:")),
        array(array("text"=>"\xf0\x9f\x93\x9dРедактировать список тем", "callback_data"=>"16:")),
        array(array("text"=>"\xe2\x9d\x8cДеактивировать приёмник", "callback_data"=>"12:")),
        generate_back_buttons($id)
        ));
    if ($answer_not_edit) {
        edit_message_buttons(null, $_SESSION["menu_message"]["result"]["chat"]["id"], $_SESSION["menu_message"]["result"]["message_id"]);
        $_SESSION["menu_message"] = answer($text,$buttons, "HTML");
    } else {
        edit_message_text($text, $buttons, "HTML");
    }
}

function deactivatereceiver12 () {
    $text = "Вы уверены, что хотите деактивировать приёмник? Он пропадет из списка доступных источников, для возвращения его придётся активировать заново.";
    $buttons = array("inline_keyboard"=>array(
        array(array("text"=>"\xe2\x98\x91\xef\xb8\x8fДа", "callback_data"=>"02dr:")),
        array(array("text"=>"\xe2\x9d\x8cОтмена", "callback_data"=>"11:{$_SESSION["id1"]}:back"))
        ));
    edit_message_text($text, $buttons, "HTML");
}

/*function availiablesourceslist13 ($breadcrumbs, $id) { //СЕЙЧАС ЭТА ФУНКЦИЯ НЕ АКТУАЛЬНА. СТАНЕТ АКТУАЛЬНА ТОЛЬКО ПОСЛЕ ЛОГИКИ ДОБАВЛЕНИЯ ИНВАЙТОВ
    $total = protopia_query("getSources", "_id title");
    $breadcrumbs = add_to_breadcrumbs ($breadcrumbs, 13);
    $text = "Вот список источников, которые можно активировать для этого приёмника. Выберите источник, чтобы редактировать его.";
    edit_message_text($text,generate_lister($breadcrumbs, 14, $total, $id));
}*/

function activesourceslist13 () {
    $almost_total = protopia_query("getReceiver", "activate_sources {_id title}", ["id: ID!" => $_SESSION["id1"]]);
    if (count($almost_total["activate_sources"])==0) {
        notification_to_callback_query ("У этого приёмника нет активированных источников");
    } else {
    add_to_breadcrumbs (13);
    $text = "Вот список источников, активированных для этого приёмника. Выберите источник, чтобы деактивировать его.";
    edit_message_text($text,generate_lister("11ds", $almost_total["activate_sources"]));
    }
}

function changethemeslist16 () {
    $almost_total = protopia_query("getReceiver", "themes {_id title}", ["id: ID!" => $_SESSION["id1"]]);
    add_to_breadcrumbs(16);
    $buttons = generate_lister("25", $almost_total["themes"]);
    array_unshift($buttons["inline_keyboard"], array(array("text"=>"\xf0\x9f\x93\x83Добавить новую тему", "callback_data"=>"17:")
        ));
    $text = "Вот список тем, связанных с этим приёмником. Выберите тему, чтобы отредактировать её.";
    edit_message_text($text, $buttons);
}

function addtheme17 () { 
    $text = "Выберите действие";
    $buttons = array("inline_keyboard"=>array(
        array(array("text"=>"\xf0\x9f\x93\x83Создать новую тему", "callback_data"=>"18:")),
        array(array("text"=>"\xf0\x9f\x93\x91Скопировать из другого приёмника", "callback_data"=>"19:")),
        array(array("text"=>"\xf0\x9f\x93\x8bСкопировать из списка тем", "callback_data"=>"23:")),
        generate_back_buttons()
        ));
    add_to_breadcrumbs(17);
    edit_message_text($text, $buttons);
}

function waitforthemename18 () {
    $_SESSION["user_state"] = "waitforthemename";
    $text = "Введите название темы";
    add_to_breadcrumbs(18);
    $buttons = array("inline_keyboard"=>array(array(array("text"=>"\xe2\x9d\x8cОтмена", "callback_data"=>"11:{$_SESSION["id1"]}:back"))));
    edit_message_text($text, $buttons);
}

function copythemereceiverlist19(){
    $total = protopia_query("getMyReceivers", "_id title");
    add_to_breadcrumbs(19);
    $text = "Выберите приёмник, из которого хотите скопировать тему";
    edit_message_text($text,generate_lister("20", $total));
}

function copythemethemelist20($receiver_id){
    $_SESSION["id2"]=$receiver_id;
    $almost_total = protopia_query("getReceiver", "themes {_id title}", ["id: ID!" => $receiver_id]);
    if (count($almost_total["themes"])==0){
        notification_to_callback_query("У выбранного приёмника нет тем");
    } else {
    add_to_breadcrumbs(20);
    $text = "Выберите тему, которую хотите скопировать.";
    edit_message_text($text,generate_lister("25cf", $almost_total["themes"]));
    }
}

function addsourcelist21 () {
    add_to_breadcrumbs(21);
    $almost_total = protopia_query("getSources", "_id title");
    $minus = protopia_query("getReceiver", "activate_sources {_id title}", ["id: ID!" => $_SESSION["id1"]]);
    $total = minus_second_array($almost_total, $minus["activate_sources"]);
    $buttons = generate_lister("11as", $total);
    array_unshift($buttons["inline_keyboard"], array(array("text"=>"\xf0\x9f\x94\xa2Сгенерировать код-приглашение", "callback_data"=>"22:")
        ));
    $text = "Выберите источник, к которому хотите подключить приёмник. Если источник принадлежит не вам, нажмите \"Сгенерировать код приглашения\"";
    edit_message_text($text, $buttons);
}

function generatereceivercode22 ($invite_code) {
    add_to_breadcrumbs (22);
    $receiver_name = protopia_query("getReceiver", "title", ["id: ID!" => "{$_SESSION["id1"]}"]);
    $text = "Код со ссылкой на приёмник {$receiver_name["title"]} готов. Нажмите кнопку \"Поделиться\" для пересылки приглашения в чат-источник. Ответить на приглашение сможет только администратор этого чата.\n\nЕсли вы не состоите в чате-источнике, либо чат-источник находится в другой соцсети, пришлите администратору код источника напрямую: {$invite_code}. Код может быть использован только один раз. \n\n Это - экран управления приглашением. Из него нельзя попасть в меню. Если хотите попасть в меню, воспользуйтесь командой /menu";
    $buttons = array("inline_keyboard"=>array(
        array(array("text"=>"\xf0\x9f\x93\xa4Поделиться", "switch_inline_query"=>"invite {$invite_code}")),
      array(array("text"=>"\xe2\x9d\x8cДеактивировать код", "callback_data"=>"Пусто"))
        ));
    $_SESSION["invites"]["$invite_code"] = $_SESSION["menu_message"]; 
    $_SESSION["menu_message"] = null;    
    edit_message_text($text, $buttons);
}

function addfromthemelist23 () {
    add_to_breadcrumbs (23);
    $total = protopia_query("getThemes", "_id title keywords receiver {_id title}"); //Достать темы
    $text = "Вот список тем, которые вам доступны. Нажмите на тему, чтобы добавить её в приёмник";
    $buttons = generate_slash_lister("25tl", "25tl", $total);
    edit_message_text($text, $buttons); 
}

function mythemeslist24 () {
    add_to_breadcrumbs(24);
    $total = protopia_query("getThemes", "_id title keywords receiver {_id title}"); //Достать темы
    $receivers = protopia_query("getMyReceivers", "_id title");//Достать список моих источников
    foreach ($total as $key => $theme) {
    foreach ($receivers as $receiver) {
        if ($receiver["_id"]==$theme["receiver"]["_id"]) {
            $total[$key]["editable"]=true;
        }
    }
    }
    //Сравнить список тем. Если в списке тем есть receiver из списка моих receiverов, добавить поле editable
    $text = "Вот список тем, которые вам доступны. Нажмите на тему, чтобы получить актуальную информацию";
    $buttons = generate_slash_lister("25", "31", $total);
    edit_message_text($text, $buttons);
}

function themesmenupoint25 ($id, $last_action = null, $answer_not_edit = false) {
    $_SESSION["id2"] = $id;
    add_to_breadcrumbs(25);
    $theme = protopia_query("getTheme", "_id title keywords is_public activate receiver {title}",["id: ID!" => $id]); 
    //$theme = temporary_get_theme($id);
    $is_public_word = is_public_word(boolval($theme["is_public"]));
    $is_activated_word = is_public_word(boolval($theme["activate"]));
    $words = special_text_list($theme["keywords"], "\xf0\x9f\x8f\xb7");
    $text = "{$last_action}Информация о теме <b>{$theme["title"]}</b>\nСвязанный приёмник: {$theme["receiver"]["title"]}\nАктивирована: {$is_activated_word}
Публична: {$is_public_word}
Список слов: {$words}";
    if (!$theme["is_public"]){ //Создание кнопки про публичность
    $is_public = array("text"=>"\xf0\x9f\x94\x93Сделать тему публичной", "callback_data"=>"25mp:");
} else {
    $is_public = array("text"=>"\xf0\x9f\x94\x92Сделать тему непубличной", "callback_data"=>"25mu:");
}
    if (!$theme["activate"]){ //Создание кнопки про активацию
    $is_activated = array("text"=>"\xf0\x9f\x94\x94Активировать тему", "callback_data"=>"25at:");
} else {
    $is_activated = array("text"=>"\xf0\x9f\x94\x95Деактивировать тему", "callback_data"=>"25dt:");
}
    $buttons = array("inline_keyboard"=>array(
        array(array("text"=>"\xe2\x9c\x8f\xef\xb8\x8fПереименовать тему", "callback_data"=>"26:")),
        array($is_public),
        array($is_activated),
        array(array("text"=>"\xe2\x9e\x95Добавить ключевое слово", "callback_data"=>"27:")),
        array(array("text"=>"\xe2\x9e\x96Удалить ключевое слово", "callback_data"=>"28:")),
        array(array("text"=>"\xe2\x9d\x8cУдалить тему", "callback_data"=>"29:")),
        generate_back_buttons()
        ));
    if ($answer_not_edit) {
        edit_message_buttons(null, $_SESSION["menu_message"]["result"]["chat"]["id"], $_SESSION["menu_message"]["result"]["message_id"]);
        $_SESSION["menu_message"] = answer($text,$buttons,"HTML");
    } else {
        edit_message_text($text, $buttons,"HTML");
    }
}

function waitfornewthemename26 () {
    $_SESSION["user_state"] = "waitfornewthemename";
    $text = "Введите новое название темы";
    add_to_breadcrumbs(26);
    $buttons = array("inline_keyboard"=>array(array(array("text"=>"\xe2\x9d\x8cОтмена", "callback_data"=>"25:{$_SESSION["id2"]}:back"))));
    edit_message_text($text, $buttons);
}

function waitforkeyword27() {
    $_SESSION["user_state"] = "waitforkeyword";
    $text = "Введите ключевое слово";
    add_to_breadcrumbs(27);
    $buttons = array("inline_keyboard"=>array(array(array("text"=>"\xe2\x9d\x8cОтмена", "callback_data"=>"25:{$_SESSION["id2"]}:back"))));
    edit_message_text($text, $buttons);
}

function deletekeywordlist28 (){
    $almost_total = protopia_query("getTheme", "keywords", ["id: ID!" => $_SESSION["id2"]]);
    if (count($almost_total["keywords"])==0) {
        notification_to_callback_query("У этой темы нет ключевых слов");
    } else {
    add_to_breadcrumbs(28);
    $text = "Выберите ключевое слово, которое хотите удалить";
    $buttons = generate_keyword_lister("25dw", $almost_total["keywords"]);
    edit_message_text($text, $buttons);
    }
}

function deletetheme29 () {
    $text = "Вы уверены, что хотите удалить тему? Данное действие невозможно обратить вспять.";
    $buttons = array("inline_keyboard"=>array(
        array(array("text"=>"\xe2\x98\x91\xef\xb8\x8fДа", "callback_data"=>"11dt:")),
        array(array("text"=>"\xe2\x9d\x8cОтмена", "callback_data"=>"25:{$_SESSION["id2"]}:back"))
        ));
    edit_message_text($text, $buttons);
}

function copytoreceiver31 ($theme_id) {
    $_SESSION["id1"] = $theme_id;
    add_to_breadcrumbs(31);
    $text = "Для того, чтобы редактировать эту тему, вам необходимо скопировать её себе в источник. В какой источник вы хотите её скопировать?";
    $total = protopia_query("getMyReceivers", "_id title");
    $buttons = generate_lister("25cr", $total);
    edit_message_text($text, $buttons);
}

function activatedinvite32 ($last_action = null) {
    
}