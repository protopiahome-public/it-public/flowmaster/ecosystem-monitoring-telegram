<?php
echo "a"; //Эти три строки кода - чтобы вебхук понял, что не надо стучать по три раза
ob_flush(); 
flush();

use Jose\Object\JWK;
use Jose\Factory\JWSFactory;

require_once("config.php");
require_once("telegram_functions.php");
require_once("graphql_functions.php");
require_once("interface_functions.php");
require_once("screens.php");
require_once("helping_functions.php");

if ($_REQUEST["secret"] != $telegram_secret)
{
	die();
}

$data = json_decode(file_get_contents('php://input'), true);
file_put_contents(__DIR__ . "/log.txt", print_r($_REQUEST, true), FILE_APPEND);
file_put_contents(__DIR__ . "/log.txt", print_r($data, true), FILE_APPEND);
protopia_auth();
if (isset($data["message"]["text"])){
$data["message"]["text"] = preg_replace("#^(\/[a-z0-9\_]+)\@.+?bot#i", "\\1", $data["message"]["text"]);
}

// Следующие строки - замена механизму сессий, потому что сессии не работали 
if ($data["callback_query"]["from"]["id"]) {
    $session_id = ("monitoringtelegram" . $data["callback_query"]["from"]["id"]);
} elseif ($data["message"]["from"]["id"]) {
    $session_id = ("monitoringtelegram" . $data["message"]["from"]["id"]);
} elseif ($data["inline_query"]["from"]["id"]) {
    $session_id = ("monitoringtelegram" . $data["inline_query"]["from"]["id"]);
} else {
    $session_id = ("monitoringtelegram" . $data["chosen_inline_result"]["from"]["id"]);
}
$session_file = __DIR__ . "/session/" . $session_id . ".json";
$_SESSION = json_decode(file_get_contents($session_file), true);
$invites = json_decode(file_get_contents(__DIR__ . "/session/" . "invites.json"), true);

//session_start();
//$_SESSION["new_test"] = "Тестирование 1"; 

/*
file_put_contents(__DIR__ . "/errors.txt", print_r($data["message"]["text"], true), FILE_APPEND);
file_put_contents(__DIR__ . "/errors.txt", print_r($data["message"]["from"]["id"], true), FILE_APPEND);
file_put_contents(__DIR__ . "/errors.txt", print_r($data["callback_query"]["from"]["id"], true), FILE_APPEND);
file_put_contents(__DIR__ . "/errors.txt", print_r($_SESSION["user_state"], true), FILE_APPEND);
*/

if (!$ecosystem_user_token && preg_match("#^\/(connect|start) (.+)$#s", $data["message"]["text"], $matches))
{
	$key4 = new JWK([
		'kty' => 'oct',
		'k'   => $ecosystem_client_secret,
	]);
	
	$login_hint_token = array(
		"sub" => $data["message"]["from"]["id"],
		"aud" => ["https://lp-telegram.kb.protopia-home.ru", "https://lp-telegram.kb.protopia-home.ru"],
		"iss" => "https://lp-telegram.kb.protopia-home.ru",
		"iat" => time(),
		"exp" => time() + 3600,
		"acr" => "telegram",
		"amr" => "opt",
	);
	$jwt = JWSFactory::createJWSToCompactJSON(
		$login_hint_token,                      // The payload or claims to sign
		$key4,                         // The key used to sign
		['alg' => 'HS256', "kid" => $ecosystem_client_id]
	);
	
	$auth_result = protopia_mutation("authorize", "auth_req_id", ["input: AuthorizeInput!" => 
	array(
		"scope" => "user",
		"user_code" => $matches[2],
//		"login_hint_token" => $jwt,
		"assertion" => $assertion_jwt,
		)
	]);
	
	$token_user_result = protopia_mutation("token", "access_token", ["input: TokenInput!" => 
		array(
			"grant_type" => "ciba",
			"auth_req_id" => $auth_result["auth_req_id"],
			"assertion" => $assertion_jwt,
		)
	]);
	
	$ecosystem_user_token = $token_user_result["access_token"];
	//answer($ecosystem_user_token);
	
	protopia_mutation("changeCurrentUser", "_id", ["input: UserCurrentInput" => [
		"telegram_id" => intval($data["message"]["from"]["id"]),
	]]);
	
	answer("Вы авторизовались!");
    mainmenu02(null, true);
}
elseif (false && !$ecosystem_user_token && preg_match("#^\/register$#s", $data["message"]["text"], $matches))
{
	protopia_mutation("registerUser", "_id", ["input: UserInput" => [
		"name" => $data["message"]["from"]["first_name"],
		"family_name" => $data["message"]["from"]["last_name"],
		"vk_id" => $data["message"]["from"]["id"],
	]]);
}
/*elseif (!$ecosystem_user_token && preg_match("#^\/#s", $data["message"]["text"], $matches)) //СЕЙЧАС ДЕАКТИВИРОВАНО
{
	answer("Добро пожаловать в Flowmaster, систему чат-мониторинга. 

Система мониторит сообщения в чатах-источниках и пересылает их в чаты-приёмники в соответствии с установленными темами.

Для того, чтобы начать пользоваться ботом, выполните 3 шага: 
1) Зарегистрируйтесь или войдите на сайт flowmaster.info;
2) Перейдите в отдел \"Войти во внешних системах\" и нажмите вариант \"Telegram\";
3) Скопируйте полученную команду и пришлите её боту.");
}*/


if (preg_match("#^\/panel$#s", $data["message"]["text"], $matches))
{
	$result = protopia_mutation("associate", "user_code", [
		"input: AuthenticatorInput" => ["authenticator_type" => "otp"],
	]);
	
	answer("Вы можете войти в веб-панель по адресу http://fm-client.kb.protopia-home.ru/login_external#{$result["user_code"]}");
}
if (preg_match("#^\/token$#s", $data["message"]["text"], $matches))
{
	answer($ecosystem_user_token);
}
elseif (preg_match("#^\/help$#s", $data["message"]["text"], $matches))
{
	answer("/themes - список доступных мне тем
/sources - список доступных мне источников
/shared_sources - список источников, расшаренных для данного приемника
/receivers - список доступных мне приемников
/monitor - сделать из чата источник
/receive - сделать из чата приемник
/create_theme <название> - создать тему для текущего приемника
/add_theme <тема> - добавить (скопировать) тему в текущий приемник
/copy_theme_to <тема> <receiver> - скопировать тему в указанный приемник
/activate_source <источник> - включить подписку приемника на источник
/activate_theme <тема> - включить подписку приемника на тему
/deactivate_source <источник> - отключить подписку приемника на источник
/deactivate_theme <тема> - отключить подписку приемника на тему
/change_theme <тема> <название> - изменить название темы
/add_word_theme <тема> <слово> - добавить слово в тему
/remove_word_theme <тема> <слово> - удалить слово из темы
/delete_theme <тема> - удалить тему
/receiver_info - получить список тем и источников для данного приемника
/share_receiver - получить ссылку на приемник
/share <приемник> - расшарить текущий чат-источник указанному приемнику
/unshare_to <приемник> - отменить расшаривание текущего чат-источника приемнику
/unshare <источник> - отменить расшаривание указанного чат-источника текущему приемнику
/cancel_share <источник> - отказаться от расшаривания указанного источника в текущий приемник
/shared_to - показать список приемников, которым расшарен текущий источник
/panel - получить код доступа в веб-панель
/connect <code> - ввести код доступа для связывания аккаунтов
/source_public - сделать источник публичным
/receiver_public - сделать приемник публичным
/theme_public <тема> - сделать тему публичной
/source_unpublic - сделать источник непубличным
/receiver_unpublic - сделать приемник непубличным
/theme_unpublic <тема> - сделать тему непубличной
/share_to <приемник> - расшарить источник в указанный приемник
");
}
elseif (preg_match("#^\/create_theme (.+)$#s", $data["message"]["text"], $matches))
{
	protopia_mutation("changeTheme", "_id", ["receiver_id: ID" => getReceiverByExternal(), "input: ThemeInput" => [
		"title" => $matches[1],
	]]);
	
	answer("Тема создана.");
}
elseif (preg_match("#^\/add_theme (.+)$#s", $data["message"]["text"], $matches))
{
	protopia_mutation("copyThemeToReceiver", "_id", [
		"_id: ID" => getIdByLinkOrTitle($matches[1], "theme"),
		"receiver_id: ID" => getReceiverByExternal(),
	]);
	
	answer("Тема скопирована.");

}
elseif (preg_match("#^\/change_theme (.+) (.+)$#s", $data["message"]["text"], $matches))
{
	protopia_mutation("changeTheme", "_id", [
		"_id: ID" => getIdByLinkOrTitle($matches[1], "theme"),
		"input: ThemeInput" => [
			"title" => $matches[2],
	]]);
	
	answer("Тема изменена.");
}
elseif (preg_match("#^\/add_word_theme (.+) (.+)$#s", $data["message"]["text"], $matches))
{
	protopia_mutation("addKeyWordToTheme", "_id", [
		"theme_id: ID" => getIdByLinkOrTitle($matches[1], "theme"),
		"keyword: String" => $matches[2],
	]);
	
	answer("Ключевое слово добавлено.");
}
elseif (preg_match("#^\/remove_word_theme (.+) (.+)$#s", $data["message"]["text"], $matches))
{
	protopia_mutation("removeKeyWordFromTheme", "_id", [
		"theme_id: ID" => getIdByLinkOrTitle($matches[1], "theme"),
		"keyword: String" => $matches[2],
	]);
	
	answer("Ключевое слово удалено.");
}
elseif (preg_match("#^\/delete_theme (.+)$#s", $data["message"]["text"], $matches))
{
	protopia_mutation("deleteTheme", "_id", [
		"_id: ID" => getIdByLinkOrTitle($matches[1], "theme"),
	]);
	
	answer("Тема удалена.");
}
elseif (preg_match("#^\/cancel_share (.+)$#s", $data["message"]["text"], $matches))
{
	protopia_mutation("unshareSource", "_id", [
		"source_id: ID" => getIdByLinkOrTitle($matches[1], "source"),
		"receiver_id: ID" => getReceiverByExternal($data["message"]["chat"]["id"]),
	]);
	
	answer("Вы отказались от источника.");
}
elseif (preg_match("#^\/shared_to$#s", $data["message"]["text"], $matches))
{
	$text = "";
	
	$source = protopia_query("getSource", "_id shared_to_receivers {_id title}", ["id: ID!" => getSourceByExternal()]);
	
	if ($source)
	{
		$text = "";
		if ($source["shared_to_receivers"])
		{
			$text .= "Приемники:\n";
			foreach ($source["shared_to_receivers"] as $receiver)
			{
				$text .= "----{$receiver["title"]}\n";
			}
		}
	}
	if (!count($source["shared_to_receivers"]))
	{
		$text = "Приемники отсутствуют.";
	}
	
	answer($text);
}
elseif (preg_match("#^\/connect_to$#s", $data["message"]["text"], $matches))
{
//	$ecosystem_client_auth = true;
	$result = protopia_mutation("associate", "user_code", ["input: AuthenticatorInput" => [
		"authenticator_type" => "oob",
		"oob_channel" => "telegram",
		"external_id" => $data["message"]["from"]["id"],
	]]);
//	$ecosystem_client_auth = false;
	answer("Пожалуйста, введите в другой системе команду \"/connect_to {$result["user_code"]}\"");
}
elseif (preg_match("#^\/source_public$#s", $data["message"]["text"], $matches))
{
	protopia_mutation("changeSource", "_id", [
		"_id: ID" => getSourceByExternal(),
		"input: SourceInput" => [
			"is_public" => true,
		]
	]);
	
	answer("Источник опубликован.");
}
elseif (preg_match("#^\/receiver_public$#s", $data["message"]["text"], $matches))
{
	protopia_mutation("changeReceiver", "_id", [
		"_id: ID" => getReceiverByExternal(),
		"input: ReceiverInput" => [
			"is_public" => true,
		]
	]);
	
	answer("Приемник опубликован.");
}
elseif (preg_match("#^\/theme_public (.+)$#s", $data["message"]["text"], $matches))
{
	protopia_mutation("changeTheme", "_id", [
		"_id: ID" => getIdByLinkOrTitle($matches[1], "theme"),
		"input: ThemeInput" => [
			"is_public" => true,
		]
	]);
	
	answer("Тема опубликована.");
}
elseif (preg_match("#^\/source_unpublic$#s", $data["message"]["text"], $matches))
{
	protopia_mutation("changeSource", "_id", [
		"_id: ID" => getSourceByExternal(),
		"input: SourceInput" => [
			"is_public" => false,
		]
	]);
	
	answer("Источник распубликован.");
}
elseif (preg_match("#^\/receiver_unpublic$#s", $data["message"]["text"], $matches))
{
	protopia_mutation("changeReceiver", "_id", [
		"_id: ID" => getReceiverByExternal(),
		"input: ReceiverInput" => [
			"is_public" => false,
		]
	]);
	
	answer("Приемник распубликован.");
}
elseif (preg_match("#^\/theme_unpublic (.+)$#s", $data["message"]["text"], $matches))
{
	protopia_mutation("changeTheme", "_id", [
		"_id: ID" => getIdByLinkOrTitle($matches[1], "theme"),
		"input: ThemeInput" => [
			"is_public" => false,
		]
	]);
	
	answer("Тема распубликована.");
}
elseif (preg_match("#^\/themes$#s", $data["message"]["text"], $matches))
{
	$themes = protopia_query("getThemes", "_id title activate is_public keywords receiver {title external_system}");
	$text = "";
	foreach ($themes as $theme)
	{
		$theme["receiver"]["title"] = $theme["receiver"]["title"] ? $theme["receiver"]["title"] . ":" : "";
		$status = $theme["activate"] ? "активна" : "неактивна";
		$is_public = $theme["is_public"] ? "публична" : "непублична";
		$text .= "{$theme["receiver"]["title"]}{$theme["receiver"]["external_system"]}:{$theme["title"]} (" . implode(", ", $theme["keywords"]) . ") [{$status}, {$is_public}]\n";
	}
	if (!count($themes))
	{
		$text = "Темы отсутствуют.";
	}
	answer($text);
}
elseif (preg_match("#^\/sources$#s", $data["message"]["text"], $matches))
{
	$sources = protopia_query("getSources", "_id title external_system is_public");
	
	$text = "";
	
	foreach ($sources as $source)
	{
		$is_public = $source["is_public"] ? "Публичный" : "Непубличный";
		$source["external_id"] = preg_replace("#^\-100#", "", $source["external_id"]);
		$text .= "{$source["title"]} ({$source["external_system"]}) [{$is_public}]\n";
	}
	
	if (!count($sources))
	{
		$text = "Источники отсутствуют.";
	}
	
	answer($text);
}
elseif (preg_match("#^\/receivers$#s", $data["message"]["text"], $matches))
{
	$sources = protopia_query("getReceivers", "_id title external_system is_public");
	
	$text = "";
	
	foreach ($sources as $source)
	{
		$is_public = $source["is_public"] ? "Публичный" : "Непубличный";
		$source["external_id"] = preg_replace("#^\-100#", "", $source["external_id"]);
		$text .= "{$source["title"]} ({$source["external_system"]}) [{$is_public}]\n";
	}
	
	if (!count($sources))
	{
		$text = "Приемники отсутствуют.";
	}
	
	answer($text);
}
elseif (preg_match("#^\/shared_sources$#s", $data["message"]["text"], $matches))
{
	$sources = protopia_query("getSharedSourcesByReceiver", "_id title external_id", ["receiver_id: ID" => getReceiverByExternal()]);

	$text = "";
	
	foreach ($sources as $source)
	{
		$source["external_id"] = preg_replace("#^\-100#", "", $source["external_id"]);
		$text .= "{$source["title"]}\n";
	}
	
	if (!count($sources))
	{
		$text = "Расшаренные источники отсутствуют.";
	}
	
	answer($text);
}
elseif (preg_match("#^\/monitor$#s", $data["message"]["text"], $matches))
{
        $source = protopia_query("getSource", "_id is_disabled", ["id: ID!" => getSourceByExternal()]);
        if (!$source["is_disabled"]&&$source["_id"]){
        answer("Этот чат уже мониторится.");
        }
        else {
	$result = protopia_mutation("changeSource", "_id", ["_id:ID" => "{$source["_id"]}",
            "input: SourceInput" => [
		"external_id" => $data["message"]["chat"]["id"],
		"external_type" => $data["message"]["chat"]["id"] == $data["message"]["from"]["id"] ? "personal_chat" : "group_chat",
		"external_system" => "telegram",
		"title" => $data["message"]["chat"]["title"],
                "is_disabled" => false,
    ]]);
	answer("Этот чат теперь мониторится. Его можно использовать как источник информации.");
        }
}
elseif (preg_match("#^\/receive$#s", $data["message"]["text"], $matches))
{
        $receiver = protopia_query("getReceiver", "_id is_disabled", ["id: ID!" => getReceiverByExternal()]);
        if (!$receiver["is_disabled"]&&$receiver["_id"]){
        answer("Этот чат уже является приёмником");
        } else {
	$receiver = protopia_mutation("changeReceiver", "_id", ["_id: ID" => $receiver["_id"], "input: ReceiverInput" => [
		"external_id" => $data["message"]["chat"]["id"],
		"external_type" => $data["message"]["chat"]["id"] == $data["message"]["from"]["id"] ? "personal_chat" : "group_chat",
		"external_system" => "telegram",
		"title" => $data["message"]["chat"]["title"],
                "is_disabled" => false
	]]);
	answer("Этот чат стал приемником");
        }
}
elseif (preg_match("#^\/activate_source (.+)$#s", $data["message"]["text"], $matches))
{
	protopia_mutation("activateSourceReceiver", "_id", [
		"source_id: String" => getIdByLinkOrTitle($matches[1], "source"),
		"id: ID" => getReceiverByExternal(),
	]);

	answer("Вы подписались на источник" . "\n\n" . getSubscriptions());
}
elseif (preg_match("#^\/activate_theme (.+)$#s", $data["message"]["text"], $matches))
{
	protopia_mutation("changeTheme", "_id", [
		"_id: ID" => getIdByLinkOrTitle($matches[1], "theme"),
		"input: ThemeInput" => [
			"activate" => true,
		]
	]);

	answer("Вы подписались на тему" . "\n\n" . getSubscriptions());
}
elseif (preg_match("#^\/deactivate_source (.+)$#s", $data["message"]["text"], $matches))
{
	protopia_mutation("deactivateSourceReceiver", "_id", [
		"source_id: String" => getIdByLinkOrTitle($matches[1], "source"),
		"id: ID" => getReceiverByExternal(),
	]);

	answer("Вы отписались от источника" . "\n\n" . getSubscriptions());
}
elseif (preg_match("#^\/deactivate_theme (.+)$#s", $data["message"]["text"], $matches))
{
	protopia_mutation("changeTheme", "_id", [
		"_id: ID" => getIdByLinkOrTitle($matches[1], "theme"),
		"input: ThemeInput" => [
			"activate" => false,
		]
	]);

	answer("Вы отписались от темы" . "\n\n" . getSubscriptions());
}
elseif (preg_match("#^\/receiver_info$#s", $data["message"]["text"], $matches))
{
	$text = getSubscriptions();
	answer($text);
}
elseif (preg_match("#^\/share_receiver$#s", $data["message"]["text"], $matches))
{
	$receiver_id = getReceiverByExternal();
	$link = createLinkById($receiver_id);
	answer("#link{$link}");
}
elseif (preg_match("#^\/share_to (.+)$#s", $data["message"]["text"], $matches))
{
	protopia_mutation("shareSource", "_id", [
		"source_id: ID" => getSourceByExternal(),
		"receiver_id: ID" => getIdByLinkOrTitle($matches[1], "receiver"),
	]);
	
	answer("Вы поделились источником.");
}
elseif (preg_match("#^\/unshare_to (.+)$#s", $data["message"]["text"], $matches))
{
	protopia_mutation("unshareSource", "_id", [
		"source_id: ID" => getSourceByExternal(),
		"receiver_id: ID" => getIdByLinkOrTitle($matches[1], "receiver"),
	]);
	
	answer("Вы отозвали доступ к источнику.");
}
elseif (preg_match("#^\/share (.+)$#s", $data["message"]["text"], $matches))
{
	protopia_mutation("shareSource", "_id", [
		"source_id: ID" => getIdByLinkOrTitle($matches[1], "source"),
		"receiver_id: ID" => getReceiverByExternal(),
	]);
	
	answer("Вы поделились источником.");
}
elseif (preg_match("#^\/unshare (.+)$#s", $data["message"]["text"], $matches))
{
	protopia_mutation("unshareSource", "_id", [
		"source_id: ID" => getIdByLinkOrTitle($matches[1], "source"),
		"receiver_id: ID" => getReceiverByExternal(),
	]);
	
	answer("Вы отозвали доступ к источнику.");
}
elseif (preg_match("#^\/share (.+)$#s", $data["message"]["text"], $matches) && isset($data["message"]["reply_to_message"]))
{
	protopia_mutation("share", "_id", [
		"source_id: ID" => getIdByLinkOrTitle($matches[1], "source"),
		"receiver_id: ID" => getReceiverByExternal($data["message"]["reply_to_message"]["from"]),
	]);
	
	answer("Вы поделились источником.");
}
elseif (preg_match("#^\/unshare (.+)$#s", $data["message"]["text"], $matches) && isset($data["message"]["reply_to_message"]))
{
	protopia_mutation("unshare", "_id", [
		"source_id: ID" => getIdByLinkOrTitle($matches[1], "source"),
		"receiver_id: ID" => getReceiverByExternal($data["message"]["reply_to_message"]["from"]),
	]);
	
	answer("Вы отозвали доступ к источнику.");
}
//Маринин код начинается
elseif (preg_match("#^\/list$#s", $data["message"]["text"], $matches))
{
        $lister_buttons = array("inline_keyboard"=>array(read_lister_layout("1c 2h 3h 4f 8e", "lister")));
	answer("Вот перелистыватель",$lister_buttons);
}
elseif (preg_match("#^\/menu$#s", $data["message"]["text"], $matches))
{   
    if ($data["message"]["chat"]["type"]!="private"){
        $source = protopia_query("getSource", "_id is_disabled", ["id: ID!" => getSourceByExternal()]);
        $receiver = protopia_query("getReceiver", "_id is_disabled", ["id: ID!" => getReceiverByExternal()]);
        if ($source["is_disabled"]||(!$source["_id"])){
            $monitor_text = "Чтобы сделать из этого чата источник, нажмите /monitor\n";
        } else {
            $monitor_text = "Этот чат является источником\n";
        }
        if ($receiver["is_disabled"]||(!$receiver["_id"])){
            $receive_text = "Чтобы сделать из этого чата приёмник, нажмите /receive\n";
        } else {
            $receive_text = "Этот чат является приёмником\n";
        }
        answer("{$monitor_text}{$receive_text}\nЧтобы настроить источник или приёмник этого чата при помощи меню, вызовите команду /menu в личной переписке с ботом");
    } else {
        mainmenu02(null, true);
    }
}
elseif (preg_match("#^\invite (+.)$#s", $data["message"]["text"], $matches))
{   
    
}
elseif (preg_match("#^\/test$#s", $data["message"]["text"], $matches)) 
{
    add_to_session ("100");
}
elseif (isset($data["message"]["text"])&&($_SESSION["user_state"] == "waitforthemename"))
{
    unset($_SESSION["user_state"]);
    shorten_breadcrumbs (16);
    $theme_id = protopia_mutation("changeTheme", "_id", ["receiver_id: ID" => $_SESSION["id1"], "input: ThemeInput" => [
		"title" => $data["message"]["text"],
	]]);
    themesmenupoint25($theme_id["_id"], "<i>Тема создана и добавлена</i>\n\n", $_SESSION["menu_message"]["result"]["message_id"]);
    file_put_contents(__DIR__ . "/errors.txt", print_r($_SESSION, true), FILE_APPEND);
}
elseif (isset($data["message"]["text"])&&($_SESSION["user_state"] == "waitfornewthemename"))
{
    unset($_SESSION["user_state"]);
    shorten_breadcrumbs (25);
    protopia_mutation("changeTheme", "_id", [
		"_id: ID" => $_SESSION["id2"],
		"input: ThemeInput" => [
			"title" => $data["message"]["text"],
	]]);
    themesmenupoint25($_SESSION["id2"], "<i>Тема успешно переименована</i>\n\n", true);
}
elseif (isset($data["message"]["text"])&&($_SESSION["user_state"] == "waitforkeyword"))
{
    unset($_SESSION["user_state"]);
    shorten_breadcrumbs (25);
        protopia_mutation("addKeyWordToTheme", "_id", [
		"theme_id: ID" => $_SESSION["id2"],
		"keyword: String" => $data["message"]["text"],
	]);
    themesmenupoint25($_SESSION["id2"], "<i>Ключевое слово добавлено</i>\n\n", true);
}
elseif (isset($data["message"]["text"])&&($_SESSION["user_state"] == "waitforreceivercode"))
{
    unset($_SESSION["user_state"]);
    shorten_breadcrumbs (11);
        $success = protopia_mutation("activateInvite", "_id", [
		"invite: String" => $data["message"]["text"],
		"source_id: ID" => $_SESSION["id1"],
	]);
    if ($success) {
    /*protopia_mutation("activateSourceReceiver", "_id", [
                "source_id: String" => "{$success["_id"]}",
		"id: ID" => "{$_SESSION["id1"]}",
	]);*/
    sourcemenupoint04($_SESSION["id1"], "<i>Приёмник добавлен</i>\n\n", true);
    } else {
    sourcemenupoint04($_SESSION["id1"], "<i>Неудача. Проверьте правильность введения кода приёмника и попробуйте ещё раз</i>\n\n", true);
    }
}
//Маринин код кончается
elseif ($data["message"]["text"])
{
	$ecosystem_client_auth = true;
	protopia_mutation("addPost", "_id", [ "input: PostInput" => [
		"external_system" => "telegram",
		"post_text" => $data["message"]["text"],
		"post_id" => $data["message"]["message_id"],
		"post_time" => date('Y-m-d\TH:i:s.uP', $data["message"]["date"]),
		"external_user_id" => $data["message"]["from"]["id"],
		"user_first_name" => $data["message"]["from"]["first_name"],
		"user_second_name" => $data["message"]["from"]["last_name"],
		"user_display_name" => $data["message"]["from"]["username"],
		"external_id" => $data["message"]["chat"]["id"],
		"external_type" => $data["message"]["chat"]["id"] == $data["message"]["from"]["id"] ? "personal_chat" : "group_chat",
		"external_system" => "telegram",
		"external_post_link" => ($data["message"]["chat"]["type"] == "supergroup") ? ("https://t.me/c/" . preg_replace("#^\-100#", "", $data["message"]["chat"]["id"]) . "/" . $data["message"]["message_id"]) : "",
		"chat_title" => $data["message"]["chat"]["title"],
		"chat_display_name" => $data["message"]["chat"]["username"],
	]]);
	$ecosystem_client_auth = false;
}
//Маринин код вновь начинается

if (preg_match("#lister#s", $data["callback_query"]["data"], $matches)){
    $callback_array = explode(":", $data["callback_query"]["data"]);
    $new_layout = regenerate_layout($callback_array[1], $callback_array[2]);
    $lister_buttons = array("inline_keyboard"=>array(read_lister_layout($new_layout, "lister")));
    //file_put_contents(__DIR__ . "/lister.txt", print_r/($lister_buttons, true), FILE_APPEND);
    //edit_message_buttons($lister_buttons);
    edit_message_buttons($lister_buttons);
    //answer("Новый layout: {$new_layout}");
    notification_to_callback_query();
    } 
elseif ($data["callback_query"]["message"]["chat"]["type"]=="private"){
    $callback_array = explode(":", $data["callback_query"]["data"]); //$callback_array[0] это следующий экран, $callback_array[1] - сопутствующий _id
    if ($callback_array[2]=="back") {//Отдельный случай для возвращения назад
    $_SESSION["breadcrumbs"] = substr($_SESSION["breadcrumbs"], 0, (strrpos($_SESSION["breadcrumbs"], "/")));
    unset($_SESSION["user_state"]);
    }
    switch ($callback_array[0]) {
        case "link":
            $result = protopia_mutation("associate", "user_code", ["input: AuthenticatorInput" => ["authenticator_type" => "otp"],]);
            edit_message_text("Ссылка для входа в вебпанель: http://fm-client.kb.protopia-home.ru/login_external#{$result["user_code"]}", null);
            mainmenu02(null, true);
            notification_to_callback_query();
            break;
        case "list":
            $layout = explode("/", $callback_array[1]);
            change_list($layout[0],$layout[1]);
            notification_to_callback_query ();
            break;
        case "invite_screen":
            break;
        case "02ds": //disable source
            $result = protopia_mutation("changeSource", "_id", [
                "_id: ID" => "{$_SESSION["id1"]}",
		"input: SourceInput" => [
			"is_disabled" => true,
		]
	]);
            if ($result) {
            mainmenu02("Иcточник успешно деактивирован\n\n"); 
            }
            else {
            mainmenu02("Произошла ошибка. Источник не деактивирован\n\n");
            }
            notification_to_callback_query ();
            break;
        case "02dr": //disable reciever
            $result = protopia_mutation("changeReceiver", "_id", [
                "_id: ID" => "{$_SESSION["id1"]}",
		"input: ReceiverInput" => [
			"is_disabled" => true,
		]
	]);
            if ($result) {
            mainmenu02("Приёмник успешно деактивирован\n\n"); 
            }
            else {
            mainmenu02("Произошла ошибка. Приёмник не деактивирован\n\n");
            }
            notification_to_callback_query ();
            break;
        case 2:
            mainmenu02();
            notification_to_callback_query ();
            break;
        case 3:
            sourceslist03();
            notification_to_callback_query ();
            break;
        case "4mp": //make public
            shorten_breadcrumbs(4);
            $answer = protopia_mutation("changeSource", "_id", [
                "_id: ID" => "{$_SESSION["id1"]}",
		"input: SourceInput" => [
			"is_public" => true,
		]
	]);
            sourcemenupoint04($_SESSION["id1"], "<i>Источник опубликован</i>\n\n");
            notification_to_callback_query ();
            break; 
        case "4mu": //make unpublic
            shorten_breadcrumbs(4);
            $answer = protopia_mutation("changeSource", "_id", [
                "_id: ID" => "{$_SESSION["id1"]}",
		"input: SourceInput" => [
			"is_public" => false,
		]
	]);
            sourcemenupoint04($_SESSION["id1"], "<i>Источник теперь не публичен</i>\n\n");
            notification_to_callback_query ();
            break;
        case "4ar": //add receiver
            shorten_breadcrumbs(4);
            protopia_mutation("shareSource", "_id", [
		"source_id: ID" => "{$_SESSION["id1"]}",
		"receiver_id: ID" => "{$callback_array[1]}",
	]);
            sourcemenupoint04($_SESSION["id1"], "<i>Приёмник добавлен</i>\n\n");
            notification_to_callback_query ();
            break;
        case "4rr": //remove receiver
            shorten_breadcrumbs (4);
            protopia_mutation("unshareSource", "title", [
		"source_id: ID" => "{$_SESSION["id1"]}",
		"receiver_id: ID" => "{$callback_array[1]}",
	]);
            sourcemenupoint04($_SESSION["id1"], "<i>Связь с приёмником разорвана.</i>\n\n");
            notification_to_callback_query ();
            break;
        case 4:
            sourcemenupoint04($callback_array[1]);
            notification_to_callback_query ();
            break;
        case 5:
            deactivatesource05();
            notification_to_callback_query ();
            break; 
        case 6:
            addrecieverlist06($_SESSION["id1"]);
            notification_to_callback_query ();
            break; 
        case 7:
            addbycode07();
            notification_to_callback_query ();
            break; 
        case 8:
            removerecieverlist08($_SESSION["id1"]);
            notification_to_callback_query ();
            break;
        case 9:
            approveremovingreceiver09($callback_array[1]);
            break;
        case 10:
            receiverslist10 ($_SESSION["breadcrumbs"]);
            notification_to_callback_query ();
            break;
        case "11mp": //make public
            shorten_breadcrumbs (11);
            protopia_mutation("changeReceiver", "_id", [
                "_id: ID" => "{$_SESSION["id1"]}",
		"input: ReceiverInput" => [
			"is_public" => true,
		]
	]);
            receivermenupoint11 ($_SESSION["id1"], "<i>Приёмник опубликован</i>\n\n");
            notification_to_callback_query ();
            break;
        case "11mu": //make unpublic
            shorten_breadcrumbs (11);
            protopia_mutation("changeReceiver", "_id", [
                "_id: ID" => "{$_SESSION["id1"]}",
		"input: ReceiverInput" => [
			"is_public" => false,
		]
	]);
            receivermenupoint11 ($_SESSION["id1"], "<i>Приёмник теперь не публичен</i>\n\n");
            notification_to_callback_query ();
            break;
        case "11ds": //deactivate source
            shorten_breadcrumbs (11);
            protopia_mutation("deactivateSourceReceiver", "_id", [
		"source_id: String" => $callback_array[1],
		"id: ID" => $_SESSION["id1"],
	]);
            receivermenupoint11 ($_SESSION["id1"], "<i>Источник деактивирован</i>\n\n");
            notification_to_callback_query ();
            break;
        case "11as": //activate source
            shorten_breadcrumbs (11);
            protopia_mutation("activateSourceReceiver", "_id", [
                "source_id: String" => "{$callback_array[1]}",
		"id: ID" => "{$_SESSION["id1"]}",
	]);
            receivermenupoint11 ($_SESSION["id1"], "<i>Источник активирован</i>\n\n");
            notification_to_callback_query ();
            break;
        case "11dt": //delete theme
            shorten_breadcrumbs (11);
            protopia_mutation("deleteTheme", "_id", [
		"_id: ID" => $_SESSION["id2"],
	]);
            receivermenupoint11 ($_SESSION["id1"], "<i>Тема удалена</i>\n\n");
            notification_to_callback_query ();
            break;
        case "11mr": //make receiver (private chat only)
            $receiver_info = protopia_query("getReceiver", "_id is_disabled", ["id: ID!" => getReceiverByExternalCallback()]);
            $receiver = protopia_mutation("changeReceiver", "_id", ["_id: ID" => $receiver_info["_id"], "input: ReceiverInput" => [
		"external_id" => $data["callback_query"]["message"]["chat"]["id"],
		"external_type" => "personal_chat",
		"external_system" => "telegram",
		"title" => $data["callback_query"]["message"]["from"]["first_name"],
                "is_disabled" => false,
	]]);
            receivermenupoint11 ($receiver["_id"], "<i>Приёмник создан\n\n</i>");
            notification_to_callback_query ();
            break;
        case 11:
            receivermenupoint11 ($callback_array[1]);
            notification_to_callback_query ();
            break;
        case 12:
            deactivatereceiver12();
            notification_to_callback_query ();
            break;
        case 13:
            activesourceslist13 ();
            notification_to_callback_query ();
            break;
        case 16:
            changethemeslist16();
            notification_to_callback_query ();
            break;
        case 17:
            addtheme17(); 
            notification_to_callback_query ();
            break;
        case 18:
            waitforthemename18();
            file_put_contents(__DIR__ . "/errors.txt", print_r($_SESSION, true), FILE_APPEND);
            break;
        case 19:
            copythemereceiverlist19();
            break;
        case 20:
            copythemethemelist20($callback_array[1]);
            break;
        case 21:
            addsourcelist21();
            notification_to_callback_query ();
            break;
        case 22:
            $invite_code = protopia_mutation("createInvite", "", [
		"receiver_id: ID" => $_SESSION["id1"]
	]);
            generatereceivercode22($invite_code);
            notification_to_callback_query ();
            break;
        case 23:
            addfromthemelist23();
            notification_to_callback_query ();
            break;
        case 24:
            mythemeslist24();
            break;        
        case "25cr": //copy (to) receiver - из списка тем
            $new_theme_id = protopia_mutation("copyThemeToReceiver", "_id", [
                "_id: ID" => $_SESSION["id1"],
                "receiver_id: ID" => $callback_array[1],
	]);
            shorten_breadcrumbs (31);
            themesmenupoint25($new_theme_id["_id"], "<i>Тема скопирована\n\n</i>");
            notification_to_callback_query ();
            break;
        case "25cf":  //copy from receiver - из опций приёмника
            $new_theme_id = protopia_mutation("copyThemeToReceiver", "_id", [
		"_id: ID" => $callback_array[1],
		"receiver_id: ID" => $_SESSION["id1"],
	]);
            shorten_breadcrumbs (16);
            themesmenupoint25($new_theme_id["_id"], "<i>Тема скопирована\n\n</i>");
            notification_to_callback_query ();
            break;
        case "25tl": //theme (from) list
            shorten_breadcrumbs (23);
            $new_theme_id = protopia_mutation("copyThemeToReceiver", "_id", [
                "_id: ID" => $callback_array[1],
                "receiver_id: ID" => $_SESSION["id1"],
	]);
            themesmenupoint25($new_theme_id["_id"], "<i>Тема скопирована\n\n</i>");
            notification_to_callback_query ();
            break;
        case "25mp": //make public
            shorten_breadcrumbs (25);
            protopia_mutation("changeTheme", "_id", [
		"_id: ID" => $_SESSION["id2"],
		"input: ThemeInput" => [
			"is_public" => true,
		]
	]);
            themesmenupoint25($_SESSION["id2"], "<i>Тема опубликована\n\n</i>");
            notification_to_callback_query ();
            break;
        case "25mu": //make unpublic
            shorten_breadcrumbs (25);
            protopia_mutation("changeTheme", "_id", [
		"_id: ID" => $_SESSION["id2"],
		"input: ThemeInput" => [
			"is_public" => false,
		]
	]);
            themesmenupoint25($_SESSION["id2"], "<i>Тема теперь не публична\n\n</i>");            
            break;
        case "25at": //activate theme 
            shorten_breadcrumbs (25);
            protopia_mutation("changeTheme", "_id", [
		"_id: ID" => $_SESSION["id2"],
		"input: ThemeInput" => [
			"activate" => true,
		]
	]);
            themesmenupoint25($_SESSION["id2"], "<i>Тема успешно активирована\n\n</i>");
            break;
        case "25dt": //deactivate theme 
            shorten_breadcrumbs (25);
            protopia_mutation("changeTheme", "_id", [
		"_id: ID" => $_SESSION["id2"],
		"input: ThemeInput" => [
			"activate" => false,
		]
	]);
            themesmenupoint25($_SESSION["id2"], "<i>Тема теперь не активна\n\n</i>");
            break;
        case "25dw": //delete word
            shorten_breadcrumbs (25);
            protopia_mutation("removeKeyWordFromTheme", "_id", [
		"theme_id: ID" => $_SESSION["id2"],
		"keyword: String" => $callback_array[1],
	]);
            themesmenupoint25($_SESSION["id2"], "<i>Ключевое слово успешно удалено\n\n</i>");
            break;
        case 25:
            themesmenupoint25($callback_array[1]);
            break;
        case 26:
            waitfornewthemename26();
            break;
        case 27:
            waitforkeyword27();
            break;
        case 28:
            deletekeywordlist28();
            break;
        case 29:
            deletetheme29();
            break;
        case 31:
            copytoreceiver31($callback_array[1]);
            break;
        case 32:
            
												  
            /*$who_pressed_button = get_chat_member(intval($data["callback_query"]["message"]["chat"]["id"]), intval($data["callback_query"]["from"]["id"]));
            answer("Здесь работает");
            if (($whothat_pressed_button["status"]!="administrator")||($whothat_pressed_button["status"]!="creator")) {
                notification_to_callback_query("Вы не можете воспользоваться этой функцией", true);
            } else {*/
            $receiver_id = getReceiverByExternalCallbackInline();
            file_put_contents("errors.txt", "Инвайт: {$callback_array[1]}\n", FILE_APPEND);
            file_put_contents("errors.txt", "ID приёмника: {$receiver_id}\n", FILE_APPEND);
                $success = protopia_mutation("activateInvite", "_id", [
		"invite: String" => $callback_array[1],
		"receiver_id: ID" => getReceiverByExternalCallbackInline(),
            ]);
            file_put_contents("errors.txt", print_r($success, true), FILE_APPEND);
                if ($success) {
                    edit_message_text_inline("Источник {$success["title"]} успешно подключен. Чтобы активировать его, воспользуйтесь меню в личном чате с ботом");
                } else {
                    edit_message_text_inline("Что-то пошло не так.");
                }
			 
            break;
        default:
            notification_to_callback_query();
            answer("Ошибка");
            break;
    }
}

if ($data["chosen_inline_result"]) {
    //answer("Чозен инлайн результ работает");
    //$invites["{$data["chosen_inline_result"]["result_id"]}"]["inline_message_id"] = $data["chosen_inline_result"]["inline_message_id"];
}

if (preg_match("#^\invite:(.+):(.+)$#s", $data["callback_query"]["data"], $matches)){

    $success = protopia_mutation("activateInvite", "_id title", [
		"invite: String" => $matches[1],
		"source_id: ID" => getSourceByExternal($invites[$matches[2]]),
	]);
	protopia_mutation("activateSourceReceiver", "_id", [
		"source_id: String" => getSourceByExternal($invites[$matches[2]]),
		"id: ID" => $success["_id"],
		]);
    if ($success) {
        answer("Приемник {$success["title"]} успешно подключен. Чтобы активировать его, воспользуйтесь меню в личном чате с ботом", null, null, $invites[$matches[2]]);
    } else {
    }
}

if (preg_match("#^\invite:(.+):(.+)#s", $data["message"]["reply_markup"]["inline_keyboard"][0][0]["callback_data"], $matches)){
    // answer ("Здесь работает");
    // answer($data["message"]["reply_markup"]["inline_keyboard"][0][0]["callback_data"]);
    // $inline_message_id = $invites["{$matches[1]}"]["inline_message_id"];
    // answer ("Инлайн-сообщение: $inline_message_id");
    // answer($data["message"]["chat"]["id"]);
    // answer($matches[2]);
    $invites[$matches[2]] = $data["message"]["chat"]["id"];
    //$text = "Редактирование работает";
    //edit_inline_message_text($text, null, null, $inline_message_id);
    } 

//КУСОК РАБОТЫ С ИНЛАЙН КВЕРЯМИ
if (preg_match("#^\invite (.+)$#s", $data["inline_query"]["query"], $matches)) {
    $receiver = protopia_query("showInvite", "_id title", ["invite: String" => "{$matches[1]}"]);
    $uniqid = uniqid();
    $messages[0] = construct_one_inline_message(
            $matches[1], 
            "К этому чату теперь можно подключить приёмник <b>{$receiver["title"]}</b>. Нажмите на кнопку, чтобы подключить приёмник к этому чату (доступно только администратору)", 
            "Поделиться приёмником {$source["title"]}", 
            array("inline_keyboard"=>array(
        array(array("text"=>"\xe2\x98\x91\xef\xb8\x8fПодключить приёмник", "callback_data"=>"invite:{$matches[1]}:{$uniqid}")),
                    )),
            "HTML");
    inline_query_answer_messages($messages);
}

file_put_contents($session_file, json_encode($_SESSION)); //Эта строка - замена механизму сессий
file_put_contents(__DIR__ . "/session/" . "invites.json", json_encode($invites));