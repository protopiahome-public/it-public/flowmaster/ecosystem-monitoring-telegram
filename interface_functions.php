<?php
/*function read_lister_layout (string $layout){ //layout - строка вида "1s 2h 3h 4f 8e", которая содержит информацию о том, как следует разложить кнопки перелистывания
    $layout_array = explode(" ",$layout);
    foreach ($layout_array as $key=>$value) { //для каждого элемента вида "1s", "2h:" и т.д.
        $button_state = substr($value, (mb_strlen($value)-1), 1);
        $number_on_button = substr($value, 0, (mb_strlen($value)-1));
        switch ($button_state){
            case "c": //current
                $result_array[$key] = array("text"=>"·{$number_on_button}·", "callback_data"=>"list:{$layout}/{$value}");
                break;
            case "h": //here
                $result_array[$key] = array("text"=>"{$number_on_button}", "callback_data"=>"list:{$layout}/{$value}");
                break;
            case "b": //backward
                $result_array[$key] = array("text"=>"‹{$number_on_button}", "callback_data"=>"list:{$layout}/{$value}");
                break;
            case "f": //forward
                $result_array[$key] = array("text"=>"{$number_on_button}›", "callback_data"=>"list:{$layout}/{$value}");
                break;
            case "s": //start
                $result_array[$key] = array("text"=>"«{$number_on_button}", "callback_data"=>"list:{$layout}/{$value}");
                break;
            case "e": //end
                $result_array[$key] = array("text"=>"{$number_on_button}»", "callback_data"=>"list:{$layout}/{$value}");
                break;
            default: 
                $result_array[$key] = array("text"=>"Ошибка", "callback_data"=>"{$layout}/{$value}");
        } 
    }
    return $result_array;
}*///ЭТО СТАРАЯ РАБОЧАЯ ВЕРСИЯ, БЕЗ СИМВОЛОВ ЮНИКОДА

function read_lister_layout (string $layout){ //layout - строка вида "1s 2h 3h 4f 8e", которая содержит информацию о том, как следует разложить кнопки перелистывания
    $layout_array = explode(" ",$layout);
    foreach ($layout_array as $key=>$value) { //для каждого элемента вида "1s", "2h:" и т.д.
        $button_state = substr($value, (mb_strlen($value)-1), 1);
        $number_on_button = substr($value, 0, (mb_strlen($value)-1));
        $emojied_number_on_button = emojis_numbers($number_on_button);
        switch ($button_state){
            case "c": //current
                $result_array[$key] = array("text"=>"\xe2\x8f\xba{$emojied_number_on_button}\xe2\x8f\xba", "callback_data"=>"list:{$layout}/{$value}");
                break;
            case "h": //here
                $result_array[$key] = array("text"=>"{$emojied_number_on_button}", "callback_data"=>"list:{$layout}/{$value}");
                break;
            case "b": //backward
                $result_array[$key] = array("text"=>"\xe2\x97\x80\xef\xb8\x8f{$emojied_number_on_button}", "callback_data"=>"list:{$layout}/{$value}");
                break;
            case "f": //forward
                $result_array[$key] = array("text"=>"{$emojied_number_on_button}\xe2\x96\xb6\xef\xb8\x8f", "callback_data"=>"list:{$layout}/{$value}");
                break;
            case "s": //start
                $result_array[$key] = array("text"=>"\xe2\x8f\xae{$emojied_number_on_button}", "callback_data"=>"list:{$layout}/{$value}");
                break;
            case "e": //end
                $result_array[$key] = array("text"=>"{$emojied_number_on_button}\xe2\x8f\xad", "callback_data"=>"list:{$layout}/{$value}");
                break;
            default: 
                $result_array[$key] = array("text"=>"Ошибка", "callback_data"=>"{$layout}/{$value}");
        } 
    }
    return $result_array;
}

function emojis_numbers($string_of_numbers){
    $array_of_numbers = str_split($string_of_numbers);
    foreach ($array_of_numbers as $number) {
        switch ($number) {
            case 0:
                $result = $result . "\x30\xef\xb8\x8f\xe2\x83\xa3";
                break;
            case 1:
                $result = $result . "\x31\xef\xb8\x8f\xe2\x83\xa3";
                break;
            case 2:
                $result = $result . "\x32\xef\xb8\x8f\xe2\x83\xa3";
                break;
            case 3:
                $result = $result . "\x33\xef\xb8\x8f\xe2\x83\xa3";
                break;
            case 4:
                $result = $result . "\x34\xef\xb8\x8f\xe2\x83\xa3";
                break;
            case 5:
                $result = $result . "\x35\xef\xb8\x8f\xe2\x83\xa3";
                break;
            case 6:
                $result = $result . "\x36\xef\xb8\x8f\xe2\x83\xa3";
                break;
            case 7:
                $result = $result . "\x37\xef\xb8\x8f\xe2\x83\xa3";
                break;
            case 8:
                $result = $result . "\x38\xef\xb8\x8f\xe2\x83\xa3";
                break;
            case 9:
                $result = $result . "\x39\xef\xb8\x8f\xe2\x83\xa3";
                break;
            default:
                $result = $result . "\x2a\xef\xb8\x8f\xe2\x83\xa3";
        }
        //answer($result);
    }  
    return $result;
}

function generate_start_lister_layout (int $number_of_pages, int $lister_size = 5) {
    $new_layout = "1c ";
    if ($number_of_pages <= 1){
        return null;
        } elseif ($number_of_pages <= $lister_size) {
            for ($i = 2; $i < $number_of_pages;$i++){
                $new_layout = $new_layout . "{$i}h ";
                }
            $new_layout = $new_layout . "{$number_of_pages}h";
        return $new_layout;
        } else {
            for ($i = 2; $i <= ($lister_size-2);$i++){ //2, потому что отнимаются конечная и предпоследняя точки. Стартовая не отнимается - начинаем с 2
                $new_layout = $new_layout . "{$i}h ";
                }
         $new_layout = $new_layout . ($lister_size-1) . "f " . $number_of_pages . "e";     
        return $new_layout;    
        }
}

function regenerate_layout (string $layout, string $button_pressed, int $lister_size = 5) {
    $number_on_button_pressed = intval(substr($button_pressed, 0, (mb_strlen($button_pressed)-1)));
    $layout_array = explode(" ", $layout);
    $away_from_center = intval(($lister_size-3)/2); //Расстояние от центра для кнопок backward и fordward
    $last_number = substr($layout_array[($lister_size-1)], 0, (mb_strlen($layout_array[($lister_size-1)])-1)); //Добыть последнее число из layout
    if (strpos($button_pressed, "h")!==false) { //Если команда here
        $new_layout = substr_replace($layout, "h", strpos($layout, "c"), 1); // убрать предыдущую c
        $new_layout = substr_replace($new_layout, ($number_on_button_pressed . "c"), strpos($new_layout, "{$number_on_button_pressed}"), strlen($button_pressed));//Заменить нажатую кнопку на c
        
    } elseif (strpos($button_pressed, "f")!==false) { //Если команда forward
        $new_layout = "1s " . ($number_on_button_pressed-($away_from_center)) . "b "; // Построить скрытые кнопки (гарантированно такого вида)
        for ($i=1;$i<$away_from_center;$i++) { // Поставить другие точки типа here (только для расклада больше 5)
            $new_layout = $new_layout . ($number_on_button_pressed-$away_from_center+$i) . "h ";
        }
        $new_layout = $new_layout . ($number_on_button_pressed) . "c "; //Поставить центральную точку
        if (($number_on_button_pressed+$away_from_center+1)==$last_number) { //Если последняя кнопка расклада совпадает с последней кнопкой списка 
            for ($i=($number_on_button_pressed+1);$i<$last_number;$i++) { //Сделать расклад, где скрытых кнопок нет - все кнопки, что должны были быть, являются here, а не f
            $new_layout = $new_layout . $i . "h ";
            } 
            $new_layout = $new_layout . $last_number . "h"; // Последнняя кнопка с командой here
        } else { //Если вложенные кнопки нужны
            for ($i=1;$i<$away_from_center;$i++) { // Поставить другие точки типа here (только для расклада больше 5)
            $new_layout = $new_layout . ($number_on_button_pressed+$i) . "h ";
            } 
            $new_layout = $new_layout . ($number_on_button_pressed+$away_from_center) . "f " . $last_number . "e";
        }
    } elseif (strpos($button_pressed, "b")!==false) {
        if (($number_on_button_pressed-$away_from_center-1)==1) {//Если первая кнопка является крайней
            for ($i=1;$i<$number_on_button_pressed;$i++) { //Сделать расклад, где скрытых кнопок нет - все кнопки, что должны были быть, являются here, а не b
                $new_layout = $new_layout . $i . "h ";
       }} else { // Сделать расклад со скрытыми кнопками
            $new_layout = "1s " . ($number_on_button_pressed-($away_from_center)) . "b ";
       }
        for ($i=1;$i<$away_from_center;$i++) { // Поставить другие точки типа here (только для расклада больше 5)
            $new_layout = $new_layout . ($number_on_button_pressed-$away_from_center+$i) . "h ";
            } 
        $new_layout = $new_layout . ($number_on_button_pressed) . "c ";//Поставить серединную точку
        for ($i=1;$i<$away_from_center;$i++) { // Поставить другие точки типа here (только для расклада больше 5)
            $new_layout = $new_layout . ($number_on_button_pressed+$i) . "h ";
            } 
        $new_layout = $new_layout . ($number_on_button_pressed+$away_from_center) . "f " . $last_number . "e";
    } elseif (strpos($button_pressed, "s")!==false) {
        $new_layout = "1c "; //Первая точка
        for ($i = 2; $i <= ($lister_size-2);$i++){ //2 - потому что отнимаются ведущая вперед и финишная точки. Стартовая точка не учитывается, т.к. начинаем с 2
                $new_layout = $new_layout . "{$i}h ";
                }
        $new_layout = $new_layout . ($lister_size-1) . "f " . $last_number . "e"; //Предпоследний и последний элементы         
    } elseif (strpos($button_pressed, "e")!==false) {
        $new_layout = "1s " . ($last_number-$lister_size+2) . "b "; //Первый и второй элементы
        for ($i = 1; $i <= ($lister_size-3);$i++){ //3 - потому что отнимаются стартовая, ведущая назад и финишная точки
                $new_layout = $new_layout . ($last_number-3+$i) . "h ";
                }
        $new_layout = $new_layout . $last_number . "c"; //Последняя точка
    }
    return $new_layout;
}

function generate_lister ($nextscreen, $total, $list_length = 3) {
    $list = array();
    if (count($total)<$list_length) {
        $current_list_length = count($total);
    } else {
        $current_list_length = $list_length;
    }
    for ($i=0;$i<$current_list_length; $i++) {
    $list["inline_keyboard"][$i][0] = array("text"=>"{$total[$i]["title"]}", "callback_data"=>"$nextscreen:{$total[$i]["_id"]}");
    if (!$total[$i]["title"]){
        $list["inline_keyboard"][$i][0]["text"] = " ";
    }
    }
    if (intval(ceil(count($total)/$list_length))>=2){ //Если количество страниц больше или равно 2
    $list["inline_keyboard"][$list_length] = read_lister_layout(generate_start_lister_layout(intval(ceil(count($total)/$list_length))),":list");
    $list["inline_keyboard"][$list_length+1] = generate_back_buttons();
    } else {
    $list["inline_keyboard"][$current_list_length] = generate_back_buttons(); 
    }
    //intval(ceil(count($total)/$list_length))
    return $list;
}

function generate_keyword_lister ($nextscreen, $total, $list_length = 3) {
    $list = array();
    if (count($total)<$list_length) {
        $current_list_length = count($total);
    } else {
        $current_list_length = $list_length;
    }
    for ($i=0;$i<$current_list_length; $i++) {
    $list["inline_keyboard"][$i][0] = array("text"=>"{$total[$i]}", "callback_data"=>"$nextscreen:{$total[$i]}");
    if (!$total[$i]["title"]){
        $list["inline_keyboard"][$i][0]["text"] = " ";
    }
    }
    if (intval(ceil(count($total)/$list_length))>=2){ //Если количество страниц больше или равно 2
    $list["inline_keyboard"][$list_length] = read_lister_layout(generate_start_lister_layout(intval(ceil(count($total)/$list_length))),":list");
    $list["inline_keyboard"][$list_length+1] = generate_back_buttons("id2");
    } else {
    $list["inline_keyboard"][$current_list_length] = generate_back_buttons("id2"); 
    }
    //intval(ceil(count($total)/$list_length))
    return $list;
}

function generate_slash_lister ($nextscreen, $secondnextscreen, $total, $list_length = 3) {
    $list = array();
    if (count($total)<$list_length) {
        $current_list_length = count($total);
    } else {
        $current_list_length = $list_length;
    }
    for ($i=0;$i<$current_list_length; $i++) {
    $list["inline_keyboard"][$i][0] = array("text"=>"{$total[$i]["title"]} | {$total[$i]["receiver"]["title"]} \xf0\x9f\x93\x91", "callback_data"=>"$secondnextscreen:{$total[$i]["_id"]}");
    if ($total[$i]["editable"]){
        $list["inline_keyboard"][$i][0] = array("text"=>"{$total[$i]["title"]} | {$total[$i]["receiver"]["title"]} \xf0\x9f\x93\x9d", "callback_data"=>"$nextscreen:{$total[$i]["_id"]}");
    }
    }
    if (intval(ceil(count($total)/$list_length))>=2){ //Если количество страниц больше или равно 2
    $list["inline_keyboard"][$list_length] = read_lister_layout(generate_start_lister_layout(intval(ceil(count($total)/$list_length))),":list");
    $list["inline_keyboard"][$list_length+1] = generate_back_buttons();
    } else {
    $list["inline_keyboard"][$current_list_length] = generate_back_buttons(); 
    }
    //intval(ceil(count($total)/$list_length))
    return $list;
}

function regenerate_lister($nextscreen, $total, $layout, $button_pressed, $list_length = 3) {
    $layout_array = explode(" ", $layout);
    $number_on_button_pressed = intval(substr($button_pressed, 0, (mb_strlen($button_pressed)-1)));
    $list = array();
    if (($layout_array[(count($layout_array)-1)])==$button_pressed){//Если нажатая кнопка - последняя
        $current_list_length = count($total)-($list_length*($number_on_button_pressed-1));
    }
    else {
        $current_list_length = $list_length;
    }
    for ($i=0;$i<$current_list_length; $i++) {
    $list["inline_keyboard"][$i][0] = array(
        "text"=>"{$total[(($number_on_button_pressed-1)*$list_length)+$i]["title"]}", 
        "callback_data"=>"$nextscreen:{$total[(($number_on_button_pressed-1)*$list_length)+$i]["_id"]}"
    );
    if (!$total[(($number_on_button_pressed-1)*$list_length)+$i]["title"]){
        $list["inline_keyboard"][$i][0]["text"] = " ";
    }}
    $list["inline_keyboard"][$current_list_length] = read_lister_layout(regenerate_layout($layout,$button_pressed),":list");
    $list["inline_keyboard"][$current_list_length+1] = generate_back_buttons();
    return $list;
}

function regenerate_slash_lister ($nextscreen, $secondnextscreen, $total, $layout, $button_pressed, $list_length = 3) {
    $layout_array = explode(" ", $layout);
    $number_on_button_pressed = intval(substr($button_pressed, 0, (mb_strlen($button_pressed)-1)));
    $list = array();
    if (($layout_array[(count($layout_array)-1)])==$button_pressed){//Если нажатая кнопка - последняя
        $current_list_length = count($total)-($list_length*($number_on_button_pressed-1));
    }
    else {
        $current_list_length = $list_length;
    }
    for ($i=0;$i<$current_list_length; $i++) {
    if ($total[(($number_on_button_pressed-1)*$list_length)+$i]["editable"]) {
    $list["inline_keyboard"][$i][0] = array(
        "text"=>"{$total[(($number_on_button_pressed-1)*$list_length)+$i]["title"]} | {$total[(($number_on_button_pressed-1)*$list_length)+$i]["receiver"]["title"]} \xf0\x9f\x93\x9d", 
        "callback_data"=>"$nextscreen:{$total[(($number_on_button_pressed-1)*$list_length)+$i]["_id"]}"
    );
    } else {
    $list["inline_keyboard"][$i][0] = array(
        "text"=>"{$total[(($number_on_button_pressed-1)*$list_length)+$i]["title"]} | {$total[(($number_on_button_pressed-1)*$list_length)+$i]["receiver"]["title"]} \xf0\x9f\x93\x91", 
        "callback_data"=>"$secondnextscreen:{$total[(($number_on_button_pressed-1)*$list_length)+$i]["_id"]}"
    );
    /*if (!$total[(($number_on_button_pressed-1)*$list_length)+$i]["title"]){
        $list["inline_keyboard"][$i][0]["text"] = " ";
    }*/
    }}
    $list["inline_keyboard"][$current_list_length] = read_lister_layout(regenerate_layout($layout,$button_pressed),":list");
    $list["inline_keyboard"][$current_list_length+1] = generate_back_buttons();
    return $list;
}

function regenerate_keyword_lister($nextscreen, $total, $layout, $button_pressed, $list_length = 3) {
    $layout_array = explode(" ", $layout);
    $number_on_button_pressed = intval(substr($button_pressed, 0, (mb_strlen($button_pressed)-1)));
    $list = array();
    if (($layout_array[(count($layout_array)-1)])==$button_pressed){//Если нажатая кнопка - последняя
        $current_list_length = count($total)-($list_length*($number_on_button_pressed-1));
    }
    else {
        $current_list_length = $list_length;
    }
    for ($i=0;$i<$current_list_length; $i++) {
    $list["inline_keyboard"][$i][0] = array(
        "text"=>"{$total[(($number_on_button_pressed-1)*$list_length)+$i]}", 
        "callback_data"=>"$nextscreen:{$total[(($number_on_button_pressed-1)*$list_length)+$i]}"
    );
    if (!$total[(($number_on_button_pressed-1)*$list_length)+$i]["title"]){
        $list["inline_keyboard"][$i][0]["text"] = " ";
    }}
    $list["inline_keyboard"][$current_list_length] = read_lister_layout(regenerate_layout($layout,$button_pressed),":list");
    $list["inline_keyboard"][$current_list_length+1] = generate_back_buttons("id2");
    return $list;
}

/*function generate_back_buttons () {
    //Если хлебные крошки длиннее 3, сделать "В меню" и "Назад"
    $breadcrumbs_array = explode("/", $_SESSION["breadcrumbs"]);
    if (isset($breadcrumbs_array[3])) {
        $buttons = array(array("text"=>"<< Назад", "callback_data"=>"{$breadcrumbs_array[count($breadcrumbs_array)-2]}:{$_SESSION["id1"]}"),array("text"=>"<< В меню", "callback_data"=>"2:"));
    } else {
        $buttons = array(array("text"=>"<< В меню", "callback_data"=>"2:"));
    }
    //Если в хлебных крошках есть меню, сделать "В меню"
    return $buttons;
}*/

function generate_back_buttons ($id_number = "id1") {
    //Если хлебные крошки длиннее 3, сделать "В меню" и "Назад"
    $breadcrumbs_array = explode("/", $_SESSION["breadcrumbs"]);
    if (isset($breadcrumbs_array[2])) {
        $buttons = array(array("text"=>"\xe2\xac\x85\xef\xb8\x8f Назад", "callback_data"=>"{$breadcrumbs_array[count($breadcrumbs_array)-2]}:{$_SESSION[$id_number]}:back"), array("text"=>"\xe2\xac\x85\xef\xb8\x8fВ меню", "callback_data"=>"2:"));
    } else {
        $buttons = array(array("text"=>"\xe2\xac\x85\xef\xb8\x8fВ меню", "callback_data"=>"2:"));
    }
    return $buttons;
}

/*function return_in_breadcrumbs ($breadcrumbs, $menupoint) {
    $breadcrumbs = substr($breadcrumbs, 0, (strpos($breadcrumbs, $menupoint)+1)); 
    return $breadcrumbs ;
}*/

function add_to_breadcrumbs (string $current_screen) {
    if (strpos($_SESSION["breadcrumbs"],("/" . $current_screen))===false) {
        $_SESSION["breadcrumbs"] = $_SESSION["breadcrumbs"] . "/" . $current_screen;
    }
    return $_SESSION["breadcrumbs"];
}

/*function webpanel_button () {
    if(!$_SESSION["user_code"]) {
        $result = protopia_mutation("associate", "user_code", ["input: AuthenticatorInput" => ["authenticator_type" => "otp"],]); //Код генерации ссылки для входа в вебпанель
        $_SESSION["user_code"] = $result["user_code"];
    }
    return array("text"=>"Войти в вебпанель", "url"=>"http://flowmaster.info?action=auth_code&code={$_SESSION["user_code"]}");
}*/

function is_public_word ($is_public) { //Трактовка is_public
    $is_public = boolval($is_public);
    if ($is_public){ 
        $is_public_word = " Да";  
    }else{
        $is_public_word = " Нет";
    }
    return $is_public_word;
}

function text_list (array $list, $bullet = "●") { //Трактовка списка
    if (count($list)==0){
        $result = " Нет";
    } else {
    foreach ($list as $listpoint) {
        $result = $result . "\n        {$bullet}{$listpoint["title"]}";
    }}
    return $result;
}

function special_text_list ($list, $bullet = "●") { //Трактовка списка
    if (count($list)==0){
        $result = " Нет";
    } else {
    foreach ($list as $listpoint) {
        $result = $result . "        {$bullet} {$listpoint}";
    }}
    return $result;
}

/*function minus_second_array ($first_array, $second_array) { //Я ХРЕН ЗНАЕТ, ПОЧЕМУ НЕ РАБОТАЕТ
    foreach ($first_array as $key => $first_array_element) {
        $should_not_stay = false;
        foreach ($second_array as $second_array_element) {
        if ($first_array_element == $second_array_element);
        $should_not_stay = true;
        }
        if ($should_not_stay) {
            unset($first_array[$key]);
        }
    }
    return $first_array;
}*/

function minus_second_array ($c2, $c1) {
    foreach($c2 as $key=>$v1){
    if(in_array($v1, $c1)){
    unset($c2[$key]);
    }
    }    
    return array_values($c2);
}

function shorten_breadcrumbs ($actionpoint) {
    if (strpos($_SESSION["breadcrumbs"], "{$actionpoint}", 0)){
    $_SESSION["breadcrumbs"] = substr($_SESSION["breadcrumbs"], 0, (strpos($_SESSION["breadcrumbs"], "$actionpoint", 0)-1));
    } else {
     $_SESSION["breadcrumbs"] = "2/$actionpoint";
    }
}

/*function temporary_get_theme ($theme_id) {
    $themes = protopia_query("getThemes", "_id title keywords is_public activate receiver {title}");
    foreach ($themes as $key => $theme) 
    {
        if ($theme["_id"]==$theme_id) {
            return $theme;
        }
    }
}*/

function getReceiverByExternalCallback()
{
	global $ecosystem_client_auth, $data;
	$ecosystem_client_auth = true;
	$result = protopia_query("getReceiverByExternal", "_id", [
		"external_id: String" => $data["callback_query"]["message"]["chat"]["id"],
		"external_type: String" => $data["callback_query"]["message"]["chat"]["id"] == $data["callback_query"]["from"]["id"] ? "personal_chat" : "group_chat",
		"external_system: String" => "telegram",
	]);
	
	$ecosystem_client_auth = false;
	return $result["_id"];
}

function recall_menu ($text, $buttons, $parsemode) { 
    answer ("{$_SESSION["menu_message"]["result"]["chat"]["id"]}");
    answer ("{$_SESSION["menu_message"]["result"]["message_id"]}");
    $ok = delete_message($_SESSION["menu_message"]["result"]["chat"]["id"], $_SESSION["menu_message"]["result"]["message_id"]);
    answer(print_r($ok,true));
    /*if (!$ok){
    edit_message_buttons(null, $_SESSION["menu_message"]["result"]["chat"]["id"], $_SESSION["menu_message"]["result"]["message_id"]);
    }*/
    $_SESSION["menu_message"] = answer($text, $buttons, $parsemode);
    //удалить предыдущие меню
    //ответить новое меню
}

function add_to_session ($what_to_add = null) {
    $_SESSION["breadcrumbs"] = $what_to_add;
}