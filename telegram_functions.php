<?php

function str_split_unicode($str, $l = 0) {
    if ($l > 0) {
        $ret = array();
        $len = mb_strlen($str, "UTF-8");
        for ($i = 0; $i < $len; $i += $l) {
            $ret[] = mb_substr($str, $i, $l, "UTF-8");
        }
        return $ret;
    }
    return preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
}

function answer($text, $buttons = null, $parse_mode = null, $chat_id = null)
{
	if (mb_strlen($text) > 3000)
	{
		$texts = str_split_unicode($text, 3000);
		foreach ($texts as $number_of_text_part => $text_part)
		{
			$result[$number_of_text_part] = answer_one($text_part, $buttons, $parse_mode, $chat_id);
		}
	}
	else
	{
		$result = answer_one($text, $buttons, $parse_mode, $chat_id);
	}
        return $result;
}

function answer_reply($text, $chat_id = null, $answer_message_id = null)
{
	global $data;
	if (!$chat_id)
	{
		$chat_id = $data["message"]["chat"]["id"];
	}
	$answer_data = array(
	'chat_id' => $chat_id,
	"text" => $text,
	//"parse_mode" => "Markdown",
	);
	if ($answer_message_id)
	{
		$answer_data["reply_to_message_id"] = $answer_message_id;
	}
	if ($parse_mode)
	{
		$answer_data["parse_mode"] = $parse_mode;
	}
	if (is_array($buttons))
	{
		$answer_data["reply_markup"]["inline_keyboard"][0] = array();
		foreach ($buttons as $command => $title)
		{
			$answer_data["reply_markup"]["inline_keyboard"][0][] = array("text" => $title, "callback_data" => $command);
		}
		$answer_data["reply_markup"] = json_encode($answer_data["reply_markup"]);
		//$answer_data["text"] = $answer_data["reply_markup"];
		//unset($answer_data["reply_markup"]);
	}
	$ch = curl_init();
    global $bot_token;
	curl_setopt($ch, CURLOPT_URL,"https://api.telegram.org/bot{$bot_token}/sendMessage");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,
				http_build_query($answer_data)
			);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$server_output = curl_exec ($ch);
	curl_close ($ch);
}

function answer_one($text, $buttons = null, $parse_mode = null, $chat_id = null)
{
	global $data;
	if (!$chat_id)
	{
            if (!$data["message"]["chat"]["id"]) {
            $chat_id = $data["callback_query"]["message"]["chat"]["id"];
            } else {
            $chat_id = $data["message"]["chat"]["id"];
            }
	}
	$answer_data = array(
	"chat_id" => $chat_id,
	"text" => $text,
	//"parse_mode" => "Markdown"
	);
	if ($parse_mode)
	{
		$answer_data["parse_mode"] = $parse_mode;
	}
	if ($buttons)
	{
		$answer_data["reply_markup"] = json_encode($buttons);
	}
	$ch = curl_init();
    global $bot_token;
	curl_setopt($ch, CURLOPT_URL,"https://api.telegram.org/bot{$bot_token}/sendMessage");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,
				http_build_query($answer_data)
			);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$server_output = curl_exec ($ch);
	curl_close ($ch);
        return (json_decode($server_output, true));
}

function answer_image($url, $chat_id = null, $parse_mode = null, $buttons = null)
{
	global $data;
	if (!$chat_id)
	{
		$chat_id = $data["message"]["chat"]["id"];
	}
	$answer_data = array(
	'chat_id' => $chat_id,
	"photo" => $url,
	//"parse_mode" => "Markdown",
	);
	if ($parse_mode)
	{
		$answer_data["parse_mode"] = $parse_mode;
	}
	if (is_array($buttons))
	{
		$answer_data["reply_markup"]["inline_keyboard"][0] = array();
		foreach ($buttons as $command => $title)
		{
			$answer_data["reply_markup"]["inline_keyboard"][0][] = array("text" => $title, "callback_data" => $command);
		}
		$answer_data["reply_markup"] = json_encode($answer_data["reply_markup"]);
		//$answer_data["text"] = $answer_data["reply_markup"];
		//unset($answer_data["reply_markup"]);
	}
	$ch = curl_init();
    global $bot_token;
	curl_setopt($ch, CURLOPT_URL,"https://api.telegram.org/bot{$bot_token}/sendPhoto");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,
				http_build_query($answer_data)
			);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$server_output = curl_exec ($ch);
	curl_close ($ch);
}

function answer_document($url, $chat_id = null, $parse_mode = null, $buttons = null)
{
	global $data;
	if (!$chat_id)
	{
		$chat_id = $data["message"]["chat"]["id"];
	}
	$answer_data = array(
	'chat_id' => $chat_id,
	"document" => $url,
	//"parse_mode" => "Markdown",
	);
	if ($parse_mode)
	{
		$answer_data["parse_mode"] = $parse_mode;
	}
	if (is_array($buttons))
	{
		$answer_data["reply_markup"]["inline_keyboard"][0] = array();
		foreach ($buttons as $command => $title)
		{
			$answer_data["reply_markup"]["inline_keyboard"][0][] = array("text" => $title, "callback_data" => $command);
		}
		$answer_data["reply_markup"] = json_encode($answer_data["reply_markup"]);
		//$answer_data["text"] = $answer_data["reply_markup"];
		//unset($answer_data["reply_markup"]);
	}
	$ch = curl_init();
    global $bot_token;
	curl_setopt($ch, CURLOPT_URL,"https://api.telegram.org/bot{$bot_token}/sendDocument");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,
				http_build_query($answer_data)
			);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$server_output = curl_exec ($ch);
//	answer($server_output);
	curl_close ($ch);
}

function answer_voice($file_id, $chat_id = null, $parse_mode = null, $buttons = null)
{
	global $data;
	if (!$chat_id)
	{
		$chat_id = $data["message"]["chat"]["id"];
	}
	$answer_data = array(
	'chat_id' => $chat_id,
	"voice" => $file_id,
	//"parse_mode" => "Markdown",
	);
	if ($parse_mode)
	{
		$answer_data["parse_mode"] = $parse_mode;
	}
	if (is_array($buttons))
	{
		$answer_data["reply_markup"]["inline_keyboard"][0] = array();
		foreach ($buttons as $command => $title)
		{
			$answer_data["reply_markup"]["inline_keyboard"][0][] = array("text" => $title, "callback_data" => $command);
		}
		$answer_data["reply_markup"] = json_encode($answer_data["reply_markup"]);
		//$answer_data["text"] = $answer_data["reply_markup"];
		//unset($answer_data["reply_markup"]);
	}
	$ch = curl_init();
    global $bot_token;
	curl_setopt($ch, CURLOPT_URL,"https://api.telegram.org/bot{$bot_token}/sendVoice");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,
				http_build_query($answer_data)
			);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$server_output = curl_exec ($ch);
	curl_close ($ch);
}

/*
 * ФУНКЦИИ ВИДА ИЗМЕНИТЬ НАЧИНАЮТСЯ ЗДЕСЬ 
 * ФУНКЦИИ ВИДА ИЗМЕНИТЬ НАЧИНАЮТСЯ ЗДЕСЬ 
 * ФУНКЦИИ ВИДА ИЗМЕНИТЬ НАЧИНАЮТСЯ ЗДЕСЬ 
 * ФУНКЦИИ ВИДА ИЗМЕНИТЬ НАЧИНАЮТСЯ ЗДЕСЬ 
 */

function edit_message_buttons ($reply_markup = null, $chat_id = null, $message_id = null) {
    global $data;
	if (!$chat_id)
	{
            if (!$data["message"]["chat"]["id"]) {
            $chat_id = $data["callback_query"]["message"]["chat"]["id"];
            } else {
            $chat_id = $data["message"]["chat"]["id"];
            }
	}
        if (!$message_id) 
        {
            if (!$data["message"]["message_id"]) {
            $message_id = $data["callback_query"]["message"]["message_id"];
            } else {
            $chat_id = $data["message"]["message_id"];
            }
        }
    $answer_data = array(
	"chat_id" => $chat_id,
	"message_id" => $message_id,
	);
    if ($reply_markup)
	{
		$answer_data["reply_markup"] = json_encode($reply_markup);
	}
	$ch = curl_init();
        global $bot_token;
	curl_setopt($ch, CURLOPT_URL,"https://api.telegram.org/bot{$bot_token}/editMessageReplyMarkup");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,
				http_build_query($answer_data)
			);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$server_output = curl_exec ($ch);
	curl_close ($ch);
}
function edit_message_text ($text, $reply_markup = null, $parse_mode = null, $chat_id = null, $message_id = null) {
    global $data;
	if (!$chat_id)
	{
            if (!$data["message"]["chat"]["id"]) {
            $chat_id = $data["callback_query"]["message"]["chat"]["id"];
            } else {
            $chat_id = $data["message"]["chat"]["id"];
            }
	}
        if (!$message_id) 
        {
            if (!$data["message"]["message_id"]) {
            $message_id = $data["callback_query"]["message"]["message_id"];
            } else {
            $chat_id = $data["message"]["message_id"];
            }
        }
    $answer_data = array(
        "text" => $text,
	"chat_id" => $chat_id,
	"message_id" => $message_id,
	);
    if ($reply_markup)
	{
		$answer_data["reply_markup"] = json_encode($reply_markup);
	}
    if ($parse_mode)
	{
		$answer_data["parse_mode"] = $parse_mode;
	}
	$ch = curl_init();
        global $bot_token;
	curl_setopt($ch, CURLOPT_URL,"https://api.telegram.org/bot{$bot_token}/editMessageText");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,
				http_build_query($answer_data)
			);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$server_output = curl_exec ($ch);
	curl_close ($ch);
        return (json_decode($server_output, true));
}

function edit_inline_message_text ($text, $reply_markup = null, $parse_mode = null, $inline_message_id = null) {
    global $data;
        if (!$inline_message_id) 
        {
            $inline_message_id = $data["inline_query"]["id"];
        }
    $answer_data = array(
        "text" => $text,
	"inline_message_id" => $inline_message_id,
	);
    if ($reply_markup)
	{
		$answer_data["reply_markup"] = json_encode($reply_markup);
	}
    if ($parse_mode)
	{
		$answer_data["parse_mode"] = $parse_mode;
	}
	$ch = curl_init();
        global $bot_token;
	curl_setopt($ch, CURLOPT_URL,"https://api.telegram.org/bot{$bot_token}/editMessageText");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,
				http_build_query($answer_data)
			);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$server_output = curl_exec ($ch);
	curl_close ($ch);
        return (json_decode($server_output, true));
}

function notification_to_callback_query (string $text = null, bool $show_alert = null, string $url = null, string $callback_query_id = null){
    global $data;
	if (!$callback_query_id)
	{
            $callback_query_id = $data["callback_query"]["id"];
	}
    $answer_data = array(
	"callback_query_id" => $callback_query_id,
	);
    if ($text){
        $answer_data ["text"] = $text;
    }
    if ($show_alert){
        $answer_data ["show_alert"] = $show_alert;
    }
    if ($url){
        $answer_data ["url"] = $url;
    }
	$ch = curl_init();
        global $bot_token;
	curl_setopt($ch, CURLOPT_URL,"https://api.telegram.org/bot{$bot_token}/answerCallbackQuery");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,
				http_build_query($answer_data)
			);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$server_output = curl_exec ($ch);
	curl_close ($ch);
}

function delete_message ($chat_id = null, $message_id = null)
{
	global $data;
	if (!$chat_id)
	{
            if (!$data["message"]["chat"]["id"]) {
            $chat_id = $data["callback_query"]["message"]["chat"]["id"];
            } else {
            $chat_id = $data["message"]["chat"]["id"];
            }
	}
        if (!$message_id) 
        {
            if (!$data["message"]["message_id"]) {
            $message_id = $data["callback_query"]["message"]["message_id"];
            } else {
            $chat_id = $data["message"]["message_id"];
            }
        }
	$answer_data = array(
            "message_id" => $message_id,
            "chat_id" => $chat_id,
	);
	$ch = curl_init();
    global $bot_token;
	curl_setopt($ch, CURLOPT_URL,"https://api.telegram.org/bot{$bot_token}/deleteMessage");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,
				http_build_query($answer_data)
			);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$server_output = curl_exec ($ch);
	curl_close ($ch);
        return (json_decode($server_output, true));
}

function construct_one_inline_message ($id, $text, $title = "Без названия", $buttons = null, $parse_mode = null) {//Конструирует одно сообщение и возвращает его. Не обджейсоненный. Только один. 
    $message_content = array (//Сделать обект вида InputMessageContent
        "message_text" => $text
    );
    if ($parse_mode) {//Запихнуть parsemode, если есть
        $message_content["parse_mode"] = $parse_mode;
    }
    $message = array (//Сделать объект вида InlineQueryResultArticle и запихнуть его внутрь массива
        "type" => "article",
        "id" => $id,
        "title" => $title,
        "input_message_content" => $message_content,
    );
    if ($buttons) {
        $message["reply_markup"] = $buttons;//Запихнуть кнопки, если надо
    }
    return $message;
}

function inline_query_answer_messages ($messages, $id = null) { //Берет в качестве $messages массив, где каждый элемент построен с помощью constuct one inline message
    $messages_string = json_encode($messages);
    if (!$id) {
        global $data;
        $id = $data["inline_query"]["id"];
    }
    $answer_data = array(
        "inline_query_id"=> $id,
	"results" => $messages_string,
	);
    $ch = curl_init();
    global $bot_token;
	curl_setopt($ch, CURLOPT_URL,"https://api.telegram.org/bot{$bot_token}/answerInlineQuery");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,
				http_build_query($answer_data)
			);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$server_output = curl_exec ($ch);
	curl_close ($ch);
        return (json_decode($server_output, true));
}

function get_chat_member ($chat_id, $user_id) {
    $answer_data = array(
        "chat_id"=> $chat_id,
	"user_id" => $user_id,
	);
    $ch = curl_init();
    global $bot_token;
	curl_setopt($ch, CURLOPT_URL,"https://api.telegram.org/bot{$bot_token}/getChatMember");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,
				http_build_query($answer_data)
			);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$server_output = curl_exec ($ch);
	curl_close ($ch);
        return (json_decode($server_output, true));
}